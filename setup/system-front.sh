#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true
export TZ="America/Los_Angeles"

cat /setup/logo.txt

apt-get update
apt-get install -y tzdata
mkdir -p $NVM_DIR
ln -fs "/usr/share/zoneinfo/America/Los_Angeles" /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

apt-get install -y build-essential software-properties-common screen nano lsof curl wget unzip openssh-client git git-lfs
curl https://raw.githubusercontent.com/creationix/nvm/$NVM_VERSION/install.sh | bash && source $NVM_DIR/nvm.sh && nvm install $NODE_VERSION && nvm alias default $NODE_VERSION && nvm use default
npm install -g yarn