# tesseract

eCommerce platform. 

## Technology used
Nuxt.js, Vuetify, Express.js, yarn, jest

## Requirements
To test locally, you will need Docker, docker-compose.
Please make sure port 8080 is available for local testing

## Deployment

TBD