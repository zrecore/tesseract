FROM ubuntu:20.04

VOLUME /app
VOLUME /setup

EXPOSE 8080

ENV NVM_DIR /usr/local/nvm
ENV NVM_VERSION v0.35.3
ENV NODE_VERSION v14.3.0
ENV NODE_PATH $NVM_DIR/$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/$NODE_VERSION/bin:$PATH


COPY ./setup/ /setup

RUN chmod +x /setup/system-front.sh
RUN bash /setup/system-front.sh
