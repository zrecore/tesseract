import Vue from 'vue'

const EventBus = new Vue()

const eventBus = {
    install(Vue, options) {
        Vue.prototype.$eventBus = EventBus
    }
}
Vue.use(eventBus)
export default eventBus