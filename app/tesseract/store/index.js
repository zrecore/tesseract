export const state = () => ({
  env: {
    BUSINESS_NAME: process.env.BUSINESS_NAME,
    BUSINESS_ADDRESS: process.env.BUSINESS_ADDRESS,
    BASE_URL: process.env.BASE_URL
  },
  cart: []
})

export const getters = {
  isAuthenticated(state) {
    return state.auth.loggedIn
  },

  loggedInUser(state) {
    return state.auth.user.user
  }
}

export const mutations = {
  addToCart(state, item) {
    state.cart.push(item);
  },
  updateItem(state, args) {

    const index = state.cart.findIndex( (i) => i.sku == args.item.sku )
    
    if (index >= 0) {
      // console.log("updateItem at index", index)
      state.cart[index].quantity = args.quantity
    }
    // console.log("STATE", state.cart);
  },
  updateCart(state, cart) {
    state.cart = cart
  },
  removeFromCart(state, { item }) {
    state.cart.splice( state.cart.indexOf( (item), 1 ) )
  },
  clearCart(state) {
    state.cart = []
  }
}
