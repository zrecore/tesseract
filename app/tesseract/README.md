# tesseract

eCommerce platform. Built on Nuxt.js, vuetify.js, express.js,

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).


## JWT Keys

See https://gist.github.com/ygotthilf/baa58da5c3dd1f69fae9
```
ssh-keygen -t rsa -b 4096 -m PEM -f jwt-rs256.key
# Don't add passphrase
openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwt-rs256.key.pub
cat jwtRS256.key
cat jwtRS256.key.pub
```

## Setup Utility

You can create accounts, (re)import base data, create .env configurations, and other administrative tasks by running `yarn setup`