process.env.NODE_ENV = process.env.NODE_ENV || 'development'
require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })

import colors from 'vuetify/es5/util/colors'

const myDomains = process.env.VUE_APP_CORS_DOMAINS;
const cspDomains = myDomains +
    "js.stripe.com " +
    "q.stripe.com" +
    "api.sendgrid.com " +
    "*.googletagmanager.com *.googleapis.com *.google-analytics.com *.gstatic.com"


export default {
  server: {
    host: "0.0.0.0",
    port: 8080
  },
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /**
   ** API server middleware
   **
   */
  serverMiddleware: [
    {path: '/api/admin', handler: '~/api/admin.js'},
    {path: "/api/auth", handler: "~/api/auth.js"},
    {path: '/api/setup', handler: '~/api/setup.js'},
    {path: '/api/product', handler: '~/api/mock/product.js'},
    {path: '/api/service', handler: '~/api/mock/service.js'},
    {path: '/api/shipping', handler: '~/api/shipping.js'},
    {path: '/api/shipping/address', handler: '~/api/shipping.js'},
    {path: '/api/country', handler: '~/api/country.js'},
    {path: '/api/region', handler: '~/api/region.js'},
    {path: '/api/send-contact', handler: '~/api/send-contact.js'},
    {path: '/api/process', handler: '~/api/process.js'},
    // {path: '/graphql', handler: '~/api/graphql.js'}
  ],
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  headers: {
      "Access-Control-Allow-Origin": myDomains,
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS",
      "Access-Control-Allow-Headers": "Content-Type",
      
      "Content-Security-Policy": "default-src 'self' 'unsafe-inline' 'unsafe-eval' 'report-sample' " + cspDomains + " data: blob:"
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    '~/plugins/event-bus.js',
    '~/plugins/axios',
    {src: '~/plugins/vuex-persist', ssr: false}
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/vuetify',
    '@nuxtjs/moment',
    ['@nuxtjs/dotenv', {filename: '.env.' + process.env.NODE_ENV}]
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    ['stripe-nuxt', process.env.STRIPE_PUBLISH_KEY],
    '@nuxtjs/auth',
    // Doc: https://github.com/nuxt/content
    '@nuxt/content',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
  },
  appCookies: {},
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api/auth/login', method: 'post', propertyName: 'token' },
          user: { url: '/api/auth/me', method: 'get', propertyName: 'data' },
          logout: false
        }
      }
    }
  },
  moment: {
    defaultTimezone: 'America/Los_Angeles',
    timezone: true
  },
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    defaultAssets: {
      font: {
        family: 'Fugaz+One'
      },
      icons: 'md'
    },
    icons: {
      iconfont: 'md'
    },
    // theme: {
    //   dark: false,
    //   themes: {
    //     dark: {
    //       primary: colors.blue.darken2,
    //       accent: colors.grey.darken3,
    //       secondary: colors.amber.darken3,
    //       info: colors.teal.lighten1,
    //       warning: colors.amber.base,
    //       error: colors.deepOrange.accent4,
    //       success: colors.green.accent3,
    //     },
    //   },
    // },
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    babel: {
      presets({isServer}) {
        return [
          [
            "@nuxt/babel-preset-app", { loose: true }
          ]
        ]
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
}
