'use strict'
process.env.NODE_ENV = process.env.NODE_ENV || 'development'
require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })

import util from 'util'
const asyncExec = util.promisify(require('child_process').exec)

import inquirer from 'inquirer'
import fs from 'fs'

import generator from 'generate-password'
import mongoose from 'mongoose'
import sgMail from '@sendgrid/mail'

import _country from './api/mock/country.js'
import _images from './api/mock/images.js'
import _product from './api/mock/product.js'
import _productFeatures from './api/mock/productFeatures.js'
import _region from './api/mock/region.js'
import _service from './api/mock/service.js'
import _shipping from './api/mock/shipping.js'
import _role from './api/mock/role.js'

import {
  Address,
  Region,
  Country,
  Images,
  Shipping,
  ProductFeatures,
  Service,
  Product,
  OrderProduct,
  OrderService,
  Order,
  RMA,
  Role,
  User,
  Session,
  Refund,
  getModel,
  startMongoose
} from './api/models'

let roles = []
_role.map((role) => {
  roles.push({name: ((role.name).charAt(0).toUpperCase() + (role.name).slice(1)).replace(/[\-_]/gi, ' '), value: role.name})
})

async function execShellCommand(command, data, answers) {
  
  console.log("execShellCommand(), returning new promise")
  let result;

  try {
    let cmd = command.cmd(answers)
    let {stdout, stderr} = await asyncExec(cmd)
    // if (stderr) {
    //   console.error("execShellCommand() asyncExec() stderror", stderr)
    // }
    let callbackResult = command.callback(stdout, data, answers)
    result = callbackResult
  } catch (err) {
    console.error("execShellCommand() asyncExec() error", err)
  }
  return result
}

var commands = [
  {
    order: 1,
    cmd: (answers) => {
      return "(yes 'y' | ssh-keygen -t rsa -b 4096 -N '' -m PEM -f jwt-rs256-" + answers.environment + ".key) && " + 
      "openssl rsa -in jwt-rs256-" + answers.environment + ".key -pubout -outform PEM -out jwt-rs256-" + answers.environment + ".pub"
    },
    callback: (output, data, answers) => {
      
      return data
    }
  }
]

var questionsAction = [
  {
    type: 'list',
    name: 'action',
    message: 'Please choose an action.',
    choices: [
      {name: 'Create a new user', value: 'new_user'},
      {name: 'Remove an existing user', value: 'remove_user'},
      {name: 'Clear all stored sessions', value: 'clear_all_sessions'},
      {name: 'Purge all orders', value: 'purge_all_orders'},
      {name: 'Generate .env file', value: 'setup_base'},
      {name: 'Import/overwrite base models', value: 'setup_base_models'},
      {name: 'Exit (Ctrl + C)', value: 'exit'}
    ],
    default: 'setup_base'
  }
]

var questionsSetup = [
  {
    type: 'list',
    name: 'environment',
    choices: [
      {name: 'Production', value: 'production'},
      {name: 'Staging', value: 'staging'},
      {name: 'Development', value: 'development'},
      {name: 'Test', value: 'test'}
    ],
    default: 'development'
  },
  {
    type: 'input',
    name: 'env.BUSINESS_NAME',
    message: 'Business Name'
  },
  {
    type: 'input',
    name: 'env.BUSINESS_ADDRESS',
    message: 'Business Address'
  },
  {
    type: 'input',
    name: 'env.BCRYPT_SALT_ROUNDS',
    message: 'BCrypt Salt Rounds',
    default: 16
  },
  {
    type: 'input',
    name: 'env.SUPPORT_EMAIL',
    message: "What e-mail should be used for communications?"
  },
  {
    type: 'input',
    name: 'env.SUPPORT_SUBJECT',
    message: "E-mail subject message.",
    default: "Support Request"
  },
  {
    type: 'input',
    name: 'env.DB_NAME',
    message: 'Mongo Database Name',
    default: 'tesseract',
  },
  {
    type: 'input',
    name: 'env.DB_HOST',
    message: 'Mongo Database Host',
    default: 'mongo'
  },
  {
    type: 'input',
    name: 'env.DB_PORT',
    message: 'Mongo Database Port',
    default: '27017'
  },
  {
    type: 'input',
    name: 'env.DB_USER',
    message: 'Mongo Database User',
    default: 'root'
  },
  {
    type: 'password',
    name: 'env.DB_PASSWORD',
    message: 'Mongo Database Password'
  },
  {
    type: 'input',
    name: 'env.BASE_URL',
    message: "What is the base URL for this checkout?",
    default: 'http://localhost:8080'
  },
  {
    type: 'input',
    name: 'env.CORS_DOMAINS',
    message: "What additional domains should be allow-listed by CORS headers? Wild card * operator supported.",
    default: 'fonts.googleapis.com fonts.gstatic.com'
  },
  {
    type: 'input',
    name: 'env.SENDGRID_API_URL',
    message: 'SendGrid API URL',
    default: 'https://api.sendgrid.com/v3/mail/send'
  },
  {
    type: 'password',
    name: 'env.SENDGRID_KEY',
    message: 'Enter your SendGrid API Key'
  },
  {
    type: 'password',
    name: 'env.STRIPE_PUBLISH_KEY',
    message: 'Enter your Stripe Publish Key.',
  },
  {
    type: 'password',
    name: 'env.STRIPE_SECRET_KEY',
    message: 'Enter your Stripe Secret Key'
  },
  {
    type: 'input',
    name: 'env.USPS_USER_ID',
    message: 'Enter your USPS User ID (if available)',
    default: ''
  }
]

var questionsUserSetup = [
  {
    type: 'input',
    name: 'USERNAME',
    message: 'Enter the new User Name'
  },
  {
    type: 'input',
    name: 'EMAIL',
    message: 'Enter the new User E-Mail',
  },
  {
    type: 'list',
    name: 'ROLE',
    message: 'Select the User Role',
    choices: roles,
    default: 'user'
  }
]

var questionsRemoveUser = [
  {
    type: 'input',
    name: 'USERNAME',
    message: 'Enter the User Name to remove'
  }
]

async function importMock(modelName, ModelSchema, mockData, beforeCreateTransform = null, overwrite = false) {
  try {
    const Model = getModel(modelName, ModelSchema)

    let existing = await Model
      .find()

    if (overwrite) {
      existing = await Model.find().deleteMany()
    } else {
      existing = await Model.find()
    }

    let allMap = await Promise.all(
      mockData.map(
        async (record) => {
          if (beforeCreateTransform) {
            record = await beforeCreateTransform(record)
          }
          
          let result = await Model.create(record)
          Promise.resolve(result)
        }
      )
    )

    existing = await Model.find()

    console.log("Imported mock data for", modelName)
  } catch (err) {
    console.error(err)
  }
}

async function setup (overwrite = false) {
  try {
    
    await importMock("Images", Images, _images, null, overwrite)
    await importMock("ProductFeatures", ProductFeatures, _productFeatures, null, overwrite)
    await importMock("Country", Country, _country, null, overwrite)
    await importMock("Region", Region, _region, null, overwrite)
    await importMock("Shipping", Shipping, _shipping, null, overwrite)
    await importMock("Role", Role, _role, null, overwrite)
    await importMock("Service", Service, _service, async (record) => {
      let mImages = getModel("Images", Images)
      let newProductFeatures = []
      let newImages = []
      
      let allImages = await Promise.all(
        record.pictures.map(async (image) => {
          image = await mImages.findOne({id: image.id})
          // console.log("beforeCreate newImage from db is", image)
          newImages.push(image._id)
        })
      )

      record.pictures = newImages
      
      Promise.resolve(record)
      return record
    }, overwrite)
    await importMock("Product", Product, _product, async (record) => {
      let mProductFeatures = getModel("ProductFeatures", ProductFeatures)
      let mImages = getModel("Images", Images)
      let newProductFeatures = []
      let newImages = []
      let allFeatures = await Promise.all(
        record.productFeatures.map(async (productFeature) => {
          productFeature = await mProductFeatures.findOne({id: productFeature.id})
          // console.log("beforeCreate productFeature from db is", productFeature)
          newProductFeatures.push(productFeature)
        })
      )
      let allImages = await Promise.all(
        record.pictures.map(async (image) => {
          image = await mImages.findOne({id: image.id})
          // console.log("beforeCreate newImage from db is", image)
          newImages.push(image._id)
        })
      )
      record.productFeatures = newProductFeatures
      record.pictures = newImages

      Promise.resolve(record)
      return record
    }, overwrite)
  } catch (err) {
    console.error("setup.js::setup()", err)
  }
}

async function setAdmin(username) {
  let result = false

  try {
    let RoleModel = getModel("Role", Role)
    let UserModel = getModel("User", User)

    let adminRole = await RoleModel.findOne({name: "admin"})
    let user = await UserModel.findOne({username: username})

    user.role = adminRole
    user.save()

    result = true
  } catch (err) {
    console.error(err)
  }
  Promise.resolve(result)
  return result
}

async function register(username, email, role = "user") {
  let newPassword = generator.generate({length: 10, numbers: true, symbols: true})

  let RoleModel = getModel("Role", Role)
  let UserModel = getModel("User", User)
  // console.log("NEW Password", newPassword)
  let userRole = await RoleModel.findOne({name: role || (process.env.DEFAULT_ROLE || "user")})

  let newUser = await UserModel.create({
    username: username,
    email: email,
    password: newPassword,
    role: userRole
  })

  // console.log("newUser", newUser)
  return newPassword
}

async function removeUser() {

  try {
    let answers = await inquirer
      .prompt(questionsRemoveUser)

    let UserModel = getModel("User", User)

    let result = await UserModel
      .findOneAndDelete({username: answers.USERNAME})
    
    console.info("User removed!")
  } catch (err) {
    console.error("setup.js::removeUser()", err)
  }

  return true
}

async function clearAllSessions() {
  try {
    let SessionModel = getModel("Session", Session)
    await SessionModel.find().deleteMany()

    console.info("All sessions cleared!")
  } catch (err) {
    console.error("setup.js::clearAllSessions()", err)
  }
}

async function purgeAllOrders() {
  try {
    const OrderProductModel = getModel("OrderProduct", OrderProduct)
    const OrderServiceModel = getModel("OrderService", OrderService)
    const RMAModel = getModel("RMA", RMA)
    const RefundModel = getModel("Refund", Refund)
    const OrderModel = getModel("Order", Order)

    await OrderModel.find().deleteMany()
    await OrderProductModel.find().deleteMany()
    await OrderServiceModel.find().deleteMany()
    await RMAModel.find().deleteMany()
    await RefundModel.find().deleteMany()

    console.info("All orders purged!")
  } catch (err) {
    console.error("setup.js::purgeAllOrders()", err)
  }
}

async function setupUser() {
  console.info("\n\nWarning: This utility *will* overwrite your existing ENV file!\n\n")
  let answers = await inquirer.prompt(questionsUserSetup)

  try {
    const username = answers.USERNAME
    const email = answers.EMAIL
    const role = answers.ROLE
    const password = await register(username, email, role)

    const sendgridKey = process.env.SENDGRID_KEY
    const data = {
      "from": process.env.SUPPORT_EMAIL,
      "to": '<' + email + '>',
      "subject": process.env.SUPPORT_SUBJECT + ": Registration",
      "text": "Thank you for registering! Here are your account details with one-time generated password.\n" +
              "Username: " + username + "\n" +
              "Password: " + password + "\n" +
              "You may log in at " + process.env.BASE_URL + "/admin-login\n",

      "html": "<p>Thank you for registering! Here are your account details with one-time generated password.</p>" + 
              "<p>Username: " + username + "</p>" + 
              "<p>Password: " + password + "</p>" +
              "<p>You may log in at <a href=\"" + process.env.BASE_URL + "/admin-login\">" + process.env.BASE_URL + "/admin-login</a></p>"
    }


    // console.table(data)
    try {
      sgMail.setApiKey(sendgridKey)
      let resp = await sgMail.send(data)

      console.info("New account registered! E-mail sent.")
    } catch (err)  {
      console.error("setup.js::setupUser(), sgMail.send() call", "(" + err.code + ") " + err.message)
    }
    
  } catch (err) {
    console.error("setup.js::setupUser() error", err)
  }
}
async function setupBase() {
  let answers = await inquirer
    .prompt(questionsSetup)

  try {
    let fileName = '.env.' + answers.environment
    let data = []
    let keys = Object.keys(answers.env)
    let i
    let exclude = ['SETUP_PASSWORD', 'INSTALL_KEY', 'CORS_DOMAINS']
    keys.map((key) => {
      if (exclude.indexOf(key) < 0) {
        data.push(key + "=" + answers.env[key])
      }
    })

    // Generate SETUP_PASSWORD
    data.push('SETUP_PASSWORD=' + generator.generate({length: 10, numbers: true, symbols: true}))
    // Generate INSTALL_KEY
    data.push("INSTALL_KEY=" + generator.generate({length: 10, numbers: true, symbols: true}))
    // Generate CORS_DOMAINS
    data.push("CORS_DOMAINS=" + answers.env.BASE_URL.replace(/^http(s)?:\/\//, '') + ' ' + answers.env.CORS_DOMAINS)
    // Add key paths
    data.push("JWT_PRIVATE_KEY_PATH=" + './jwt-rs256-' + answers.environment + '.key')
    data.push("JWT_PUBLIC_KEY_PATH=" + './jwt-rs256-'+answers.environment+'.pub')

    let sortMethod = (a,b) => {
      a.order > b.order ?
        -1 :
        (b.order > a.order ?
          1 :
          0
        )
    }
    let sortedCommands = commands.sort(sortMethod)
    let shellResult;
    await Promise.all(sortedCommands.map(async (command) => {
      data = await execShellCommand(command, data, answers)
      fs.writeFileSync(fileName, data.join("\n"))
      console.info("\t\t---> Saved to", fileName)
      Promise.resolve(true)
    }))
    

  } catch (err) {
    console.error(err)
  }
}

async function requestAction() {
  let answers = await inquirer.prompt(questionsAction)

  switch (answers.action) {
    case 'new_user':
      
      await setupUser()
      break
    case 'remove_user':
      await removeUser()
      break
    case 'clear_all_sessions':
      await clearAllSessions()
      break
    case 'purge_all_orders':
      await purgeAllOrders()
      break
    case 'setup_base':
      await setupBase()
      break
    case 'setup_base_models':
      await setup(true)
      break
    case 'exit':
    default:
      
      break
  }
  process.exit(0)
}


console.info(process.env.NODE_ENV)
if (process.env.NODE_ENV == 'development') {
  let logo = fs.readFileSync('./logo.txt')
  console.log("\n" + logo)
}
startMongoose()
requestAction()