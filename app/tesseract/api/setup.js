var express = require('express');
var bodyParser = require('body-parser');
var axios = require('axios');
var rateLimit = require('express-rate-limit');
var cors = require('cors');
var dotenv = require('dotenv');
var compression = require('compression');
var helmet = require('helmet');
var bcrypt = require('bcrypt');

const mongoose = require('mongoose');

import {
  Order,
  OrderProduct,
  OrderService,
  Address,
  Region,
  Country,
  Images,
  Shipping,
  Service,
  Product,
  ProductFeatures,
  Role,
  User,
  RMA,
  Refund,
  getModel,
  startMongoose
} from './models';

import _country from './mock/country.js';
import _images from './mock/images.js';
import _product from './mock/product.js';
import _productFeatures from './mock/productFeatures.js';
import _region from './mock/region.js';
import _service from './mock/service.js';
import _shipping from './mock/shipping.js';
import _role from './mock/role.js';

const {
  expressCspHeader,
  INLINE,
  NONE,
  SELF
} = require('express-csp-header');

// const sgMail = require('@sendgrid/mail');
const limiter = rateLimit({
    windowsMs: 30 * 60 * 1000, // 30 minutes
    max: 100//1 // limit each IP to 1 requests per windowMs
});
const app = express();

dotenv.config();

app.use(helmet());
app.use(cors());
app.options(
  process.env.CORS_DOMAINS,
  cors()
);

app.use(limiter);
app.use(expressCspHeader({
  "default-src": [NONE],
  "image-src": [SELF]
}));
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());

async function importMock(modelName, ModelSchema, mockData, beforeCreateTransform = null, overwrite = false) {
  try {
    const Model = getModel(modelName, ModelSchema);

    let existing = await Model
      .find();

    if (overwrite) {
      existing = await Model.find().deleteMany();
    } else {
      existing = await Model.find();
    }

    let allMap = await Promise.all(
      mockData.map(
        async (record) => {
          if (beforeCreateTransform) {
            record = await beforeCreateTransform(record);
            console.log("record returned from beforeCreateTransform()", record);
          }
          
          let result = await Model.create(record);
          Promise.resolve(result);
        }
      )
    );

    existing = await Model.find();
    console.log("Imported " + modelName + ":", existing);
  } catch (err) {
    console.error(err);
  }
}

async function setup (overwrite = false) {
    // const CountryModel = getModel("Country", Country);
    // const ImagesModel = getModel("Images", Images);
    // const ProductModel = getModel("Product", Product);
    // const ProductFeatures = getModel("ProductFeatures", ProductFeatures);
    // const RegionModel = getModel("Region", Region);
    // const ServiceModel = getModel("Service", Service);
    // const ShippingModel = getModel("Shipping", Shipping);

    startMongoose();

    await importMock("Images", Images, _images, null, overwrite);
    await importMock("ProductFeatures", ProductFeatures, _productFeatures, null, overwrite);
    await importMock("Country", Country, _country, null, overwrite);
    await importMock("Region", Region, _region, null, overwrite);
    await importMock("Shipping", Shipping, _shipping, null, overwrite);
    await importMock("Service", Service, _service, null, overwrite);
    await importMock("Role", Role, _role, null, overwrite);
    await importMock("Product", Product, _product, async (record) => {
      let mProductFeatures = getModel("ProductFeatures", ProductFeatures);
      let mImages = getModel("Images", Images);
      let newProductFeatures = [];
      let newImages = [];
      let allFeatures = await Promise.all(
        record.productFeatures.map(async (productFeature) => {
          productFeature = await mProductFeatures.findOne({id: productFeature.id});
          // console.log("beforeCreate productFeature from db is", productFeature);
          newProductFeatures.push(productFeature);
        })
      );
      let allImages = await Promise.all(
        record.pictures.map(async (image) => {
          image = await mImages.findOne({id: image.id});
          // console.log("beforeCreate newImage from db is", image);
          newImages.push(image);
        })
      );
      record.productFeatures = newProductFeatures;
      record.pictures = newImages;
      // console.log("beforeCreate record will be", record);
      Promise.resolve(record);
      return record;
    }, overwrite);
}

async function setAdmin(username) {
  let result = false;
  startMongoose()

  try {
    let RoleModel = getModel("Role", Role);
    let UserModel = getModel("User", User);

    let adminRole = await RoleModel.findOne({name: "admin"});
    let user = await UserModel.findOne({username: username});

    user.role = adminRole;
    user.save();

    result = true;
  } catch (err) {
    console.error(err);
  }
  Promise.resolve(result);
  return result;
}
/**
 * Send a contact request via SendGrid API (e-mail).
 * Expects name, email, and message strings.
 * 
 */
app.get(
  '/',
  [
    // check("countryCode", "countryCode max length exceeded").isLength({max: 2}),
    // check("email", "email field is not a valid e-mail").isEmail(),
    // check("message", "message field is invalid (min 3 chars, max 1000 chars)").isLength({min: 3, max: 1000})
  ],
  async (req, res) => {
    let result;
    let data;
    console.log("query", req.query);
    if (
      typeof process.env.INSTALL_KEY == 'undefined'
      
    ) {

      console.error("Install key not set!");
      res.status(401).json({errors: ["Install key not set!"]});

      return;
    } else if (
      process.env.INSTALL_KEY &&
      req.query.key !== process.env.INSTALL_KEY
    ) {
      console.error("Wrong install key!");
      res.status(401).json({errors: ["Wrong install key!"]});

      return;
    }


    console.log('/setup')

    await setup(req.query.overwrite ? true : false);
    result = res
      .status(200)
      .json({data: "ok", description: "Setup complete!"})
    return result;
});

app.get(
  '/set-admin',
  [],
  async (req, res) => {
    let result;
    let data;
    console.log("query", req.query);
    if (
      typeof process.env.INSTALL_KEY == 'undefined'
      
    ) {

      console.error("Install key not set!");
      res.status(401).json({errors: ["Install key not set!"]});

      return;
    } else if (
      process.env.INSTALL_KEY &&
      req.query.key !== process.env.INSTALL_KEY
    ) {
      console.error("Wrong install key!");
      res.status(401).json({errors: ["Wrong install key!"]});

      return;
    }


    console.log('/setup/set-admin')
    let username = req.query.username;

    if (username) {
      let rolechange = await setAdmin(username);

      if (rolechange) {
        result = res
          .status(200)
          .json({data: "ok", description: "setAdmin complete!"});
      } else {
        result = res
          .status(400)
          .json({errors: ["Failed to set admin role on requested user."]});
      }
    } else {
      result = res
        .status(422) // Unprocessable entity, missing username
        .json({errors: ["username parameter invalid."]});
    }
    return result;
});

app.get(
  '/purge-orders',
  [],
  async (req, res) => {
    let data;
    console.log("query", req.query);
    if (
      typeof process.env.INSTALL_KEY == 'undefined'
      
    ) {

      console.error("Install key not set!");
      res.status(401).json({errors: ["Install key not set!"]});

      return;
    } else if (
      process.env.INSTALL_KEY &&
      req.query.key !== process.env.INSTALL_KEY
    ) {
      console.error("Wrong install key!");
      res.status(401).json({errors: ["Wrong install key!"]});

      return;
    }

    startMongoose();

    const OrderProductModel = getModel("OrderProduct", OrderProduct);
    const OrderServiceModel = getModel("OrderService", OrderService);
    const RMAModel = getModel("RMA", RMA);
    const RefundModel = getModel("Refund", Refund);
    const OrderModel = getModel("Order", Order);

    await OrderModel.find().deleteMany();
    await OrderProductModel.find().deleteMany();
    await OrderServiceModel.find().deleteMany();
    await RMAModel.find().deleteMany();
    await RefundModel.find().deleteMany();

    res.status(200).json({"message": "ok"});

    console.log('/setup/purge-orders')
    
    return;
});

console.log("setup.js LOADED")
module.exports = {
    path: '/api/setup',
    handler: app
}