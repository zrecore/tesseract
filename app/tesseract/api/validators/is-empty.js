export default function isEmpty(value) {
    return (typeof value === 'undefined') && !value;
}