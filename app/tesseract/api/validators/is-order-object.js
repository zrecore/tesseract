import hasAddressProperty from './has-address-property.js';
import isEmpty from './is-empty.js';
import isDate from './is-date.js';
import isID from './is-id.js';

// console.log("hasAddressProperty required!", hasAddressProperty);
/**
 * [isOrderObject checks if the value is a valid database ID (_id) field]
 * @param  {Object}  value [Valid Order object]
 * @return {Boolean}       [Returns true if it is an Order object.]
 */

export default function isOrderObject(value) {
  // console.log("hasAddressProperty", value.billing, hasAddressProperty(value, 'billing'))
  // console.log(
  //   "billing", (isID(value['billing']) || hasAddressProperty(value, 'billing')) + "\n",
  //   "shipping", (isID(value['shipping']) || hasAddressProperty(value, 'shipping')) + "\n",
  //   "products|services", (value.products.length || value.services.length) + "\n",
  //   "payment_type", value.payment_type.length > 0, "\n",
  //   "payment_gateway", value.payment_gateway + "\n",
  //   "payment_id", value.payment_id + "\n",
  //   "shipping_cost", (+value.shipping_cost), '>=', 0.00, "\n",
  //   "item_cost", (+value.item_cost), '>=', 0.00, "\n",
  //   "total_cost", (+value.total_cost / 100).toFixed(2), '==', (+value.shipping_cost) + (+value.item_cost), "\n",
  //   "currency", value.currency + "\n",
  //   "receipt_email", value.receipt_email + "\n",
  //   "order_date", isDate(value.order_date) + "\n",
  //   "ship_method (set?)", value['ship_method'] + "\n",
  //   "ship_method (valid?)", (isID(value['ship_method']) || isShippingMethod(value['ship_method'])), "\n",
  //   "ship_date", typeof value['ship_date'], "value", value['ship_date'], !isEmpty(value['ship_date']), "\n"//,
  //   // "ship_method", (!isEmpty(value['ship_tracking']) ? value['ship_tracking'].length > 4 && value['ship_tracking'].length < 32 : true), "\n"
  // );
  let is_order = (
      (isID(value['billing']) || hasAddressProperty(value, 'billing')) &&
      (isID(value['shipping']) || hasAddressProperty(value, 'shipping')) &&
      (value.products.length || value.services.length) &&
      value.payment_type.length > 0 &&
      value.payment_gateway &&
      value.payment_id &&
      (+value.shipping_cost) >= 0.00 &&
      (+value.item_cost) >= 0.00 &&
      (+value.total_cost / 100).toFixed(2) == (+value.shipping_cost) + (+value.item_cost) &&
      value.currency &&
      value.receipt_email &&
      isDate(value.order_date) &&
      value['ship_method'] &&
      (isID(value['ship_method']) || isShippingMethod(value['ship_method'])) &&
      (!isEmpty(value['ship_date']) ? isDate(value['ship_date']) : true) &&
      (!isEmpty(value['ship_tracking']) ? value['ship_tracking'].length > 4 && value['ship_tracking'].length < 32 : true)
  );


  // @TODO create a comprehensive rule check for the _id field
  return is_order;
};