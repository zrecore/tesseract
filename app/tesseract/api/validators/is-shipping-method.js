const _shipping = require('../mock/shipping.js');
/**
 * [isDate checks if the value is a valid shipping method]
 * @param  {String}  value [Valid shipping.name string]
 * @return {Boolean}       [Returns true if it is a shiping.name value.]
 */
export default function isShippingMethod(value) {
  let found = _shipping.filter(ship => value == ship.value);

  return found && found.length > 0;
};