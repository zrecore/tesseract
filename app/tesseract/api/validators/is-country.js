import countries from '../mock/country.js';

export default function isCountry(value) {
  const found = countries.filter(
    (country) => {
      // console.log('isCountry() value', value);
      return value.code == country.code &&
          value.country == country.country
    }
  );

  return found && found.length > 0;
}