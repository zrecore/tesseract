/**
 * [isDate checks if the value is a valid Date()]
 * @param  {String}  value [Valid Date() string]
 * @return {Boolean}       [Returns true if it is a Date() string.]
 */
export default function isDate(value) {
  return !isNaN(Date.parse(value));
};