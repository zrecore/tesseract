/**
 * [isID checks if the value is a valid database ID (_id) field]
 * @param  {String}  value [Valid ID string]
 * @return {Boolean}       [Returns true if it is an ID string.]
 */
export default function isID(value) {
  // console.log('isID', value);
  // @TODO create a comprehensive rule check for the _id field
  return (value && value._id && value._id.length > 23) || typeof value == 'string' && value.length > 23;
};