import isAddressString from './is-address-string.js';
import isCountry from './is-country.js';
import isEmpty from './is-empty.js';

export default function hasAddressProperty(orderObject, propertyName) {
  return typeof orderObject[propertyName] != 'undefined' &&
         orderObject[propertyName] &&
         isAddressString(orderObject[propertyName]['address1']) &&
         // address2 is optional
         isCountry(orderObject[propertyName]['country']) &&
        !isEmpty(orderObject[propertyName]['fullName']) &&
        !isEmpty(orderObject[propertyName]['email']) &&
        !isEmpty(orderObject[propertyName]['cityTown']) &&
        !isEmpty(orderObject[propertyName]['state']) &&
        !isEmpty(orderObject[propertyName]['postalZipCode']);
};