export default function isAddressString(address) {
    return typeof address == 'string' &&
           address.length > 0
}