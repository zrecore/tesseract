var express = require('express');
var bodyParser = require('body-parser');
var axios = require('axios');
var rateLimit = require('express-rate-limit');
var cors = require('cors');
var dotenv = require('dotenv');
var compression = require('compression');
var helmet = require('helmet');
var fs = require('fs');
var bcrypt = require('bcrypt');
var generator = require('generate-password');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
import {
  Order,
  Address,
  Region,
  Country,
  Images,
  Shipping,
  Service,
  Product,
  ProductFeatures,
  User,
  Session,
  Role,
  getModel,
  startMongoose
} from './models';

startMongoose();

const {
  check,
  validationResult
} = require('express-validator');

const {
  sanitizeBody
} = require('express-validator');
const {
  expressCspHeader,
  INLINE,
  NONE,
  SELF
} = require('express-csp-header');

const sgMail = require('@sendgrid/mail');
const limiter = rateLimit({
    windowsMs: 30 * 60 * 1000, // 30 minutes
    max: 10//1 // limit each IP to 1 requests per windowMs
});
const app = express();

dotenv.config();

app.use(helmet());
app.use(cors());
app.options(
  process.env.CORS_DOMAINS,
  cors()
);

app.use(limiter);
app.use(expressCspHeader({
  "default-src": [NONE],
  "image-src": [SELF]
}));
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());

let privateKey = fs.readFileSync(process.env.JWT_PRIVATE_KEY_PATH, 'utf8');
let publicKey = fs.readFileSync(process.env.JWT_PUBLIC_KEY_PATH, 'utf8');

async function register(username, email) {
  let newPassword = generator.generate({length: 10, numbers: true, symbols: true});

  let RoleModel = getModel("Role", Role);
  let UserModel = getModel("User", User);
  // console.log("NEW Password", newPassword);
  let userRole = await RoleModel.findOne({name: process.env.DEFAULT_ROLE || "user"});

  let newUser = await UserModel.create({
    username: username,
    email: email,
    password: newPassword,
    role: userRole
  });

  // console.log("newUser", newUser);
  return newPassword;
}

app.post(
  '/login',
  [
    check("username", "username field is empty").isLength({min: 1}),
    // check("email", "email field is not a valid e-mail").isEmail(),
    check("password", "password field is invalid (min 8 chars, max 1024 chars)").isLength({min: 3, max: 1024})
  ],
  async (req, res) => {
    let result;
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    
    if (isValid) {
      let UserModel = getModel("User", User)
      let username = req.body.username;
      let password = req.body.password;
      let record = await UserModel.findOne({username: username});

      if (record) {
        let matchPassword = await record.comparePassword(password);

        if (matchPassword) {
          // Generate JWT
          
          let token = jwt.sign({_id: record._id}, privateKey, { algorithm: 'RS256' });

          let SessionModel = getModel("Session", Session);
          let data = {token: token};

          await SessionModel.create(data);

          result = res.status(200).json({
            token: token,
            status: 'success'
          });

        } else {
          result = res.status(401).json({errors: ["Invalid username or password."]})
        }
      } else {
        result = rs.status(401).json({errors: ["Invalid username or password."]});
      }
      // 
    }
    return result;
});

app.get(
  '/me',
  expressJwt({
    secret: publicKey,
    algorithms: ['RS256']
  }),
  async (req, res) => {
    // console.log("/me user", req.user);
    // console.log("headers", req.headers);
    // if (!req.headers['authorization']) {
    //   res.status(401).json({errors: ["Access Denied"]});
    //   return;
    // }
    // let token = (req.headers['authorization']).replace(/^Bearer /, '');

    // let verified = await jwt.verify(token, publicKey);
    let UserModel = getModel("User", User);
    let user = (await UserModel.findById(req.user._id));
    // // let SessionModel = getModel("Session", Session);
    user.password = '';
    // console.log("user", user);
    // console.log("verified", verified);

    return res.status(200).json({data: {user}});
});

// app.get('/prune', async (req, res) => {
//   let UserModel = getModel("User", User)
//   let SessionModel = getModel("Session", Session)

//   let result = await UserModel.find().deleteMany();
//   await SessionModel.find().deleteMany();

//   return res.status(200).json(result);
// })
/**
 * Send a contact request via SendGrid API (e-mail).
 * Expects name, email, and message strings.
 * 
 */
app.post(
  '/register',
  [
    check("username", "username field is empty").isLength({min: 1}),
    check("email", "email field is not a valid e-mail").isEmail(),
    // check("password", "password field is invalid (min 8 chars, max 1024 chars)").isLength({min: 3, max: 1024})
  ],
  async (req, res) => {
    let result;
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    
    if (isValid) {
      const email = req.body.email;
      const username = req.body.username;
      const password = await register(username, email);

      const sendgridKey = process.env.SENDGRID_KEY;
      const data = {
        "from": process.env.SUPPORT_EMAIL,
        "to": '<' + email + '>',
        "subject": process.env.SUPPORT_SUBJECT + ": Registration",
        "text": "Thank you for registering! Here are your account details with one-time generated password.\n" +
                "Username: " + username + "\n" +
                "Password: " + password + "\n" +
                "You may log in at " + process.env.BASE_URL + "/admin-login\n",

        "html": "<p>Thank you for registering! Here are your account details with one-time generated password.</p>" + 
                "<p>Username: " + username + "</p>" + 
                "<p>Password: " + password + "</p>" +
                "<p>You may log in at <a href=\"" + process.env.BASE_URL + "/admin-login\">" + process.env.BASE_URL + "/admin-login</a></p>"
      };


      // console.table(data);
      sgMail.setApiKey(sendgridKey);
      sgMail
        .send(data)
        .then((resp) => {
          result = res.status(202).json({message: 'Processed POST HTTP method /auth.'});
          // console.log("send-contact API OK", resp);
        })
        .catch((error) => {

          result = res.status(500).json({errors: ["auth API Error: (" + error.code + ") " + error.message]});
          // console.log("send-contact API Error", error.code, error.message);
        });
      return result;
    } else {
        // res.status(400); // Bad Request (missing value? Malformed syntax?)
        result = res
          .status(422) // Unprocessable entity (has values, but values may be invalid)
          .json({
            errors: errors.array()
          });
    }
    return result;
});
console.log("auth.js LOADED")
module.exports = {
    path: '/api/auth',
    handler: app
}