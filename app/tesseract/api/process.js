var express = require('express');
var bodyParser = require('body-parser');
var axios = require('axios');
var rateLimit = require('express-rate-limit');
var cors = require('cors');
var dotenv = require('dotenv');
var compression = require('compression');
var helmet = require('helmet');

var bcrypt = require('bcrypt');

import {
  Order,
  OrderProduct,
  OrderService,
  Address,
  Region,
  Country,
  Images,
  Shipping,
  Service,
  Product,
  ProductFeatures,
  getModel,
  startMongoose
} from './models';

const isDate = require('./validators/is-date.js');
const isShippingMethod = require('./validators/is-shipping-method.js');

startMongoose();

const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const {
  check,
  validationResult
} = require('express-validator');

const {
  sanitizeBody
} = require('express-validator');
const {
  expressCspHeader,
  INLINE,
  NONE,
  SELF
} = require('express-csp-header');

const sgMail = require('@sendgrid/mail');
const limiter = rateLimit({
    windowsMs: 30 * 60 * 1000, // 30 minutes
    max: 10//1 // limit each IP to 1 requests per windowMs
});
const app = express();

dotenv.config();

app.use(helmet());
app.use(cors());
app.options(
  process.env.CORS_DOMAINS,
  cors()
);

app.use(limiter);
app.use(expressCspHeader({
  "default-src": [NONE],
  "image-src": [SELF]
}));
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());

const countryData = require('./mock/country').default;
const regionData = require('./mock/region').default;
const productData = require('./mock/product').default;
const serviceData = require('./mock/service').default;
const shippingData = require('./mock/shipping').default;

// console.log('productData is', productData);
const getHydratedCart = (items) => {
  
  const cart = items.filter((i) => i);
  const products = [
    ...productData.filter((d) => d.sku),
    ...serviceData.filter((d) => d.sku)
  ]
  // console.info(products)
  const hydrated = cart.map((item) => {
    // console.info("cart.map at item sku", item.sku)

    const hydratedItem = products.find((p) => {
      // console.log("\t\t...p.sku == item.sku", p.sku == item )
      return p.sku === item.sku
    })
    if (typeof hydratedItem !== 'undefined') {
      hydratedItem.quantity = item.quantity
    }
    return hydratedItem
  }).filter((h) => h)

  return hydrated
};

const getHydratedProductsAndServices = async (items) => {
  const cart = items.filter((i) => i);
  let ProductModel = getModel("Product", Product);
  let ServiceModel = getModel("Service", Service);
  let OrderProductModel = getModel("OrderProduct", OrderProduct);
  let OrderServiceModel = getModel("OrderService", OrderService);
  // const products = [
  //   ...productData.filter((d) => d.sku)
  // ];
  // const services = [
  //   ...serviceData.filter((d) => d.sku)
  // ];
  // console.log("getHydratedProductsAndServices cart", cart);
  
  let skus = [];
  cart.map((item) => skus.push(item.sku));

  const products = await ProductModel.find().where('sku').in(skus).lean();
  const services = await ServiceModel.find().where('sku').in(skus).lean();
  // console.info('products from db', products);
  // console.info('services from db', services);

  let hydratedProducts = await Promise.all(cart.map( async(item) => {
    // console.info("cart.map at item sku", item.sku)
    let newItem = null;
    const hydratedItem = products.find((p) => {
      // console.log("\t\t...p.sku == item.sku", p.sku == item )
      return p.sku === item.sku;
    })

    if (typeof hydratedItem !== 'undefined') {
      hydratedItem.quantity = item.quantity;
      // console.log("hydrated to array", hydratedItem);
      let data = Object.assign({}, hydratedItem);
      let id = (data._id);
      data._id = null;
      data.__v = null;
      data.inStock = null;
      data.productFeatures = null;
      delete data._id;
      delete data.__v;
      delete data.inStock;
      delete data.productFeatures
      data.product = hydratedItem;
      // console.log("hydrated product is now", data);
      newItem = await OrderProductModel.create( data );
    }
    Promise.resolve(newItem);
    return newItem;
  }));
  hydratedProducts = hydratedProducts.filter((h) => h);

  let hydratedServices = await Promise.all(cart.map( async (item) => {
    // console.info("cart.map at item sku", item.sku)
    let newItem = null
    const hydratedItem = services.find((p) => {
      // console.log("\t\t...p.sku == item.sku", p.sku == item )
      return p.sku === item.sku;
    })
    if (typeof hydratedItem !== 'undefined') {
      hydratedItem.quantity = item.quantity;
      let data = Object.assign({}, hydratedItem);
      data._id = null;
      data.__v = null;
      delete data._id;
      delete data.__v;
      data.service = hydratedItem;
      newItem = await OrderServiceModel.create(data);
    }
    Promise.resolve(newItem);
    return newItem;
  }))

  hydratedServices = hydratedServices.filter((h) => h);

  // console.log("hydrated products", hydratedProducts);
  // console.log("hydrated services", hydratedServices);
  return {hydratedProducts, hydratedServices};
};

const getCartTotal = (hydratedCart) => {
  let total = 0.00
  hydratedCart.map((item) => {

    total += (+item.price) * (+item.quantity)
  })
  return total.toFixed(2)
}
const getCartTotalWithShipping = (cartTotal, shippingMethod) => {
  let total = (+cartTotal) + (+getShippingCost(shippingMethod))
  return (+total).toFixed(2)
}
const getShippingCost = (shippingMethod) => {
  let total = 0.00
  if (shippingData.length > 0) {
    const shippingObject = shippingData.filter((method) => {
      return method.value == shippingMethod
    })
    if (shippingObject.length > 0) {
      total += (+shippingObject[0].price)
    }
  }
  return (+total).toFixed(2)
}

app.get(
  '/',
  [],
  async (req, res, next) => {
    
    res.status(200).json({message: "ok"})
    // var OrderModel = getModel("Order", Order);
    // const data = await OrderModel.find();
    // // data.deleteMany();
    // res.status(200).json(data);
  }
);
/**
 * Send a contact request via SendGrid API (e-mail).
 * Expects name, email, and message strings.
 * 
 */
app.post(
  '/stripe-card-intent',
  [
    check("items", "items field expects an array").custom((value) => {
      return Array.isArray(value)
    }),
    check("items", "items field array must have at least one (1) item").custom((value) => {
      return value.length > 0
    }),
    check("billing", "optional billing field expects an object").custom((value) => {
      return value ? typeof value == 'object' : true
    }),
    check("billing", "optional billing.country must be set").custom((value) => {
      return value ? value.country : true
    }),
    check("billing", "optional billing.fullName must be set").custom((value) => {
      return value ? value.fullName : true
    }),
    check("billing", "optional billing.address1 must be set").custom((value) => {
      return value ? value.address1 : true
    }),
    check("billing", "optional billing.address2 must be between 1 and 1000 characters").custom((value) => {
      return value && value.address2.length >= 1 && value.address2.length <= 1000 ? value.country : true
    }),
    check("billing", "optional billing.cityTown must be set").custom((value) => {
      return value ? value.cityTown : true
    }),
    check("billing", "optional billing.state must be set").custom((value) => {
      return value ? value.state : true
    }),
    check("billing", "optional billing.postalZipCode must be set").custom((value) => {
      return value ? value.postalZipCode : true
    }),
    check("shipping", "shipping field expects an object").custom((value) => {
      return typeof value == 'object'
    }),
    check("shipping", "shipping.method is required").custom((value) => {
      return value.method
    }),
    check("shipping", "shipping.country is required").custom((value) => {
      return value.country
    }),
    check("shipping", "shipping.fullName is required").custom((value) => {
      return value.fullName
    }),
    check("shipping", "shipping.address1 is required").custom((value) => {
      return value.address1
    }),
    check("shipping", "optional shipping.address2 property size must be between 1 and 1000 characters").custom((value) => {
      return value.address2 ? value.address2.length >= 1 && value.address2.length <= 1000 : true
    }),
    check("shipping", "shipping.cityTown field is required").custom((value) => {
      return value.cityTown
    }),
    check("shipping", "shipping.state is required").custom((value) => {
      return value.state
    }),
    check("shipping", "shipping.postalZipCode is required").custom((value) => {
      return value.postalZipCode
    }),
    check("email", "billingEmail field is not a valid e-mail").optional().isEmail(),
    check("email", "email field is not a valid e-mail").isEmail()
  ],
  async (req, res) => {
    let result;
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    
    if (isValid) {
      const data = req.body;
      const cart = getHydratedCart(data.items);
      // console.log("/api/process/stripe-card-intent, hydrated items are", getHydratedCart(data.items));
      const total = getCartTotal(cart);
      const shipping = getShippingCost(data.shipping.method);
      // Stripe uses a whole integer, where the last two digits are the cents.
      const totalWithShipping = ((+getCartTotalWithShipping(total, data.shipping.method)).toFixed(2) * 100).toFixed(0);

      // console.log("shipping via " + data.shipping, shipping);
      // console.log("cart total is", total);
      // console.log("grand total is", totalWithShipping);

      const shippingAddress = {
        city: data.shipping.cityTown,
        country: data.shipping.country,
        line1: data.shipping.address1,
        line2: data.shipping.address2,
        postal_code: data.shipping.postalZipCode,
        state: data.shipping.state
      };

      const billingAddress = data.billing ?
        {
          name: data.billing.fullName,
          email: data.billingEmail,
          address: {
            city: data.billing.cityTown,
            country: data.billing.country,
            line1: data.billing.address1,
            line2: data.billing.address2,
            postal_code: data.billing.postalZipCode,
            state: data.billing.state
          }
        } :
        shippingAddress;

      const customer = await stripe.customers.create({
        // @TODO use billing address
        address: billingAddress,
        shipping: {
          name: data.shipping.fullName,
          address: shippingAddress
        },
        email: data.billingEmail ? data.billingEmail : data.email
      });

      const paymentIntent = await stripe.paymentIntents.create({
        amount: totalWithShipping,
        currency: "usd",
        customer: customer.id,
        receipt_email: data.billingEmail ? data.billingEmail : data.email,
        shipping: {
          carrier: data.shipping.method,
          name: data.shipping.fullName,
          address: shippingAddress
        },
        metadata: {
          items: JSON.stringify(data.items)
        }
        
      });
      // console.log("paymentIntent", paymentIntent);


      var OrderModel = getModel("Order", Order);
      var AddressModel = getModel("Address", Address);
      var CountryModel = getModel("Country", Country);
      var RegionModel = getModel("Region", Region);
      var ShippingModel = getModel("Shipping", Shipping);
      // console.log("Getting hydrated products and services");
      let {hydratedProducts, hydratedServices} = await getHydratedProductsAndServices(data.items);
      // console.log('products', hydratedProducts);
      // console.log('services', hydratedServices);
      let _billing = data.billing ? data.billing : data.shipping;
      let _shipping = data.shipping;

      // console.log("_billing before", _billing);
      // console.log("_shipping before", _shipping);

      let _billing_country_code = (_billing.country);
      let _billing_state_code = (_billing.state);
      let _shipping_country_code = (_shipping.country);
      let _shipping_state_code = (_shipping.state);

      _billing.country = await CountryModel.findOne({code: _billing_country_code});
      _billing.state = await RegionModel.findOne({code: _billing_state_code, countryCode: _billing_country_code});
      _billing.email = data.billingEmail ? data.billingEmail : data.email;

      _shipping.country = await CountryModel.findOne({code: _shipping_country_code});
      _shipping.state = await RegionModel.findOne({code: _shipping_state_code, countryCode: _shipping_country_code});
      _shipping.email = data.email;

      // console.log("_billing", _billing);
      // console.log("_shipping", _shipping);

      let billingObject = await AddressModel.create(_billing);
      let shippingObject = await AddressModel.create(_shipping);
      let shipMethodObject = await ShippingModel.findOne({value: data.shipping.method});

      let clientOrder = await OrderModel.create({
        order_date: new Date(),
        billing: billingObject,
        shipping: shippingObject,
        products: hydratedProducts,
        services: hydratedServices,
        payment_type: paymentIntent.payment_method_types,
        payment_gateway: "stripe",
        payment_id: paymentIntent.id,
        shipping_cost: shipping,
        item_cost: total,
        total_cost: totalWithShipping,
        currency: "usd",
        receipt_email: paymentIntent.receipt_email,
        ship_method: shipMethodObject,
      });

      // console.log("client order", clientOrder);

      result = res
        .status(200)
        .json({
          "clientSecret": paymentIntent.client_secret
        });
    } else {
        // res.status(400); // Bad Request (missing value? Malformed syntax?)
        result = res
          .status(422) // Unprocessable entity (has values, but values may be invalid)
          .json({
            errors: errors.array()
          });
    }
    return result;
});
console.log("process.js LOADED");
module.exports = {
    path: '/api/process',
    handler: app
}