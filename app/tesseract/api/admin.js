var express = require('express');
var bodyParser = require('body-parser');
var axios = require('axios');
var rateLimit = require('express-rate-limit');
var cors = require('cors');
var dotenv = require('dotenv');
var compression = require('compression');
var helmet = require('helmet');
const moment = require('moment');

dotenv.config();

var bcrypt = require('bcrypt');
var jwt = require('express-jwt');
var fs = require('fs');
var publicKey = fs.readFileSync(process.env.JWT_PUBLIC_KEY_PATH);

const isDate = require('./validators/is-date.js').default;
const isID = require('./validators/is-id.js').default;
const isOrderObject = require('./validators/is-order-object.js').default;

import {
  Address,
  Region,
  Country,
  Images,
  Shipping,
  ProductFeatures,
  Service,
  Product,
  OrderProduct,
  OrderService,
  Order,
  RMA,
  Refund,
  getModel,
  startMongoose
} from './models';

const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const {
  check,
  validationResult
} = require('express-validator');

const {
  sanitizeBody
} = require('express-validator');
const {
  expressCspHeader,
  INLINE,
  NONE,
  SELF
} = require('express-csp-header');

const sgMail = require('@sendgrid/mail');
const limiter = rateLimit({
    windowsMs: 30 * 60 * 1000, // 30 minutes
    max: 500 // limit each IP to 500 requests per windowMs
});
const app = express();

app.use(helmet());
app.use(cors());
app.options(
  process.env.CORS_DOMAINS,
  cors()
);

app.use(limiter);
app.use(expressCspHeader({
  "default-src": [NONE],
  "image-src": [SELF]
}));
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());

app.use(jwt({
  secret: publicKey,
  algorithms: ['RS256']
}));

startMongoose();

app.get(
  '/',
  [],
  async (req, res) => {
    
    res.status(200).json({message: "ok"})
  }
);

/**
 * /order endpoint
 *
 * Params:
 *
 * skip          Number   Default is 0
 * limit         Number   Default is 20
 * sortKey       String   Default is 'order_date'
 * sortOrder     Number   Default is -1
 * status        String   Optional. Can be 'unshipped', 'rma', 'refund'
 * beforeDate    Date
 * afterDate     Date
 */
app.get(
  '/order',
  [
    check("_id", "_id field is unprocessable").optional().isLength({min: 23}),
    check("skip", "skip field must be a positive integer").optional().custom((value) => {return (+value) >= 0}),
    check("limit", "limit field must be a positive integer").optional().custom((value) => {return (+value) > 0}),
    check("sortKey", "sortKey field is unprocessable (1 > sortKey < 100)").optional().isLength({min:1, max:100}),
    check("status", "status field is unprocessable. Must be 'unshipped', 'rma', or 'refund'")
      .optional()
      .custom((value) => {
        return (['unshipped', 'rma', 'refund'].filter(v => value == v)).length > 0
      }),
    check("beforeDate", "beforeDate field is unprocessable")
      .optional()
      .custom(isDate),
    check("afterDate", "afterDate field is unprocessable")
      .optional()
      .custom(isDate)
  ],
  async (req, res) => {

    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!

    if (!isValid) {
      res
        .status(422) // Unprocessable entity (has values, but values may be invalid)
        .json({
          errors: errors.array()
        });
      return;
    }
    
    try {
      let result, data;

      let ImagesModel = getModel("Images", Images);
      let AddressModel = getModel("Address", Address);
      let ProductFeaturesModel = getModel("ProductFeatures", ProductFeatures);
      let ProductModel = getModel("Product", Product);
      let ServiceModel = getModel("Service", Service);
      let OrderProductModel = getModel("OrderProduct", OrderProduct);
      let OrderServiceModel = getModel("OrderService", OrderService);
      let OrderModel = getModel("Order", Order);
      let CountryModel = getModel("Country", Country);
      let RegionModel = getModel("Region", Region);
      let ShippingModel = getModel("Shipping", Shipping);
      let RMAModel = getModel("RMA", RMA);
      let RefundModel = getModel("Refund", Refund);
      
      let skip = req.query.skip || 0;
      let limit = req.query.limit || 5;
      let sortKey = req.query.sortKey || 'order_date';
      let sortOrder = req.query.sortOrder || -1; // Default DESC
      let sort = {};
      sort[sortKey] = sortOrder;

      let populateFields = {
        products: {
          path: 'products',
          populate: [
            {
              path: 'product',
              populate: [
                {path: 'productFeatures'},
                {path: 'pictures'}
              ]
            }
          ]
        },
        shipping: {
          path: 'shipping',
          populate: [
            {path: 'country'},
            {path: 'state'}
          ]
        },
        billing: {
          path: 'billing',
          populate: [
            {path: 'country'},
            {path: 'state'}
          ]
        },
        rma_request: {
          path: 'rma_request',
          populate: [
            {path: 'rma_product'}
          ]
        },
        refund_request: {
          path: 'refund_request',
          populate: [
            {path: 'orderProduct', populate: 'product'},
            {path: 'orderService', populate: 'service'}
          ]
        }
      };

      if (req.query._id) {
        let id = req.query._id;
        data = await OrderModel
          .findById(id)
          .populate(populateFields.products)
          .populate(populateFields.billing)
          .populate(populateFields.shipping)
          .populate('services')
          .populate('ship_method')
          .populate(populateFields.rma_request)
          .populate('refund');
        res.status(200).json(data)
      } else {
        let query = null;

        if (req.query.afterDate) {
          query = {
            "order_date": {
              "$gte": req.query.afterDate
            }
          };
        }

        if (req.query.beforeDate) {
          if (!query) query = {"order_date": {}};

          query["order_date"]["$lte"] = req.query.beforeDate;
        }

        if (req.query.status) {
          if (!query) query = {};
          switch(req.query.status) {
            case 'unshipped':
              query["ship_tracking"] = {
                "$exists": false
              };
              query["full_refund_id"] = {
                "$exists": false
              }
              break;
            case 'rma':
              query = {};
              query["rma_request"] = {
                "$exists": true,
                "$not": {
                  "$size": 0
                }
              };
              break;
            case 'refund':
              query["$or"] = [
                {refund_request: {
                  "$exists": true,
                  "$not": {
                    "$size": 0
                  }
                }},
                {full_refund_id: {
                  "$exists": true
                }}
              ];
              break;
          }
        }
        // console.log("query?", query);

        let count = await OrderModel.countDocuments(query || null);
        data = await OrderModel
          .find(query || null)
          .sort(sort)
          .skip((+skip))
          .limit((+limit))
          .populate('products')
          .populate('services');
        res.status(200).json({count, data});
      }

      return result;
    } catch (err) {
      console.error("HTTP GET /order error", err);
      res.status(500).json({errors: [err]});
    }
  }
);

app.put(
  '/order',
  [
    check('order', 'Order._id field is not valid').custom(isID),
    check('order', 'Order object is not valid.').custom(isOrderObject)
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    // console.log("req.body", req.body)
    if (!isValid) {
      res
        .status(422) // Unprocessable entity (has values, but values may be invalid)
        .json({
          errors: errors.array()
        });
      return;
    }

    let ImagesModel = getModel("Images", Images);
    let AddressModel = getModel("Address", Address);
    let ProductFeaturesModel = getModel("ProductFeatures", ProductFeatures);
    let ProductModel = getModel("Product", Product);
    let ServiceModel = getModel("Service", Service);
    let OrderProductModel = getModel("OrderProduct", OrderProduct);
    let OrderServiceModel = getModel("OrderService", OrderService);
    let OrderModel = getModel("Order", Order);
    let CountryModel = getModel("Country", Country);
    let RegionModel = getModel("Region", Region);
    let ShippingModel = getModel("Shipping", Shipping);
    let RMAModel = getModel("RMA", RMA);
    let RefundModel = getModel("Refund", Refund);

    // console.log("Saving order", req.body.order);
    try {
      let existingOrder = await OrderModel.findByIdAndUpdate(req.body.order._id, req.body.order);
      res.status(200).json({status: "ok", order: existingOrder});
    } catch (err) {
      console.error("HTTP PUT /order error", err);
      res.status(500).json({errors: [err]});
    }
  }
);
app.delete(
  '/order/rma',
  [
    check("_id", "_id field is unprocessable. Min length is 24 chars.").isLength({min: 24})
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    // console.log("req.body", req.body)
    if (!isValid) {
      res
        .status(422) // Unprocessable entity (has values, but values may be invalid)
        .json({
          errors: errors.array()
        });
      return;
    }

    try {
      // Check for the specified RMA ID.
      let OrderModel = getModel("Order", Order);
      let RMAModel = getModel("RMA", RMA);
      let rmaId = req.body._id;
      let existingRMA = RMAModel.findById(rmaId);

      if (existingRMA) {
        let existingOrder = await OrderModel.findOne({rma_request: {_id: rmaId}})
        // console.log("Got existingOrder with RMA", rmaId, existingOrder);
        let deleteResult = await RMAModel.findByIdAndRemove(rmaId);
        let rmaRequests = existingOrder.rma_request.filter((id) => {return id != rmaId});
        existingOrder.rma_request = rmaRequests;
        await existingOrder.save()
        // console.log("rma_request array is now", rmaRequests);
        res.status(200).json({data: deleteResult});
      } else {
        res.status(404).json({errors: ["OrderProduct Not Found"]});
      }
    } catch (err) {
      console.error("HTTP DELETE /admin/order/rma error", err);
      res
        .status(500)
        .json({
          errors: [err]
        });
    }

  }
);

app.put(
  '/order/rma',
  [
    check("order_id", "order_id field is unprocessable. Min length is 24 chars.").isLength({min: 24}),
    check("order_product_id", "order_product_id field is unprocessable. Min legth is 24 chars." ).optional().isLength({min: 24})
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    // console.log("req.body", req.body)
    if (!isValid) {
      res
        .status(422) // Unprocessable entity (has values, but values may be invalid)
        .json({
          errors: errors.array()
        });
      return;
    }

    let orderId = req.body.order_id;
    let orderProductId = req.body.order_product_id;

    let ProductModel = getModel("Product", Product);
    let OrderProductModel = getModel("OrderProduct", OrderProduct);
    let OrderModel = getModel("Order", Order);
    let RMAModel = getModel("RMA", RMA);
    let existingOrder = await OrderModel
      .findById(orderId)
      .populate({
        path: 'rma_request',
        populate: {
          path: 'rma_product'
        }
      })
      .populate({
        path: 'products',
        populate: 'product'
      });
    try {
      // console.log("existingOrder " + orderId, existingOrder);
      if (existingOrder) {
        let rmaExistingCount = 0;
        let matchingOrderProducts;

        // count quantity of the requested product in the order
        let totalProductCount = 0;
        // console.log("existingOrder.products length", existingOrder.products.length);
        // console.log("existingOrder.products check for id", orderProductId);
        let existingProductsToCount = await existingOrder
          .products
          .filter((orderProduct) => {

            let matching = orderProduct._id == orderProductId;
            // console.log("orderProduct " + orderProduct._id + " matches " + orderProductId + "?", matching);
            if (matching) {
              // console.log("Found existing product to count:", orderProduct.quantity)
              totalProductCount += (+orderProduct.quantity);
            }
            return matching;
          });

        // console.log("existingProductsToCount", existingProductsToCount.length);
        if (existingProductsToCount.length > 0) {
          // cycle through each Order "orderProduct" model, 
          // and count the amount of existing RMAS per order product
          existingProductsToCount.map((orderProduct) => {
            // console.log("checking orderProduct", orderProduct);
            existingOrder.rma_request.map((existingRMA) => {
              let matchesRMAProduct = ("" + existingRMA.rma_product._id) == ("" + orderProduct.product._id);

              // console.log("checking existingRMA", existingRMA);
              // console.log("existingRMA.rma_product", existingRMA.rma_product);
              // console.log("orderProduct.product._id", orderProduct.product._id);
              // console.log("check existing rma product ID " + existingRMA.rma_product._id + " == OrderProduct.product._id " + orderProduct.product._id, matchesRMAProduct);

              if ( matchesRMAProduct ) {
                ++rmaExistingCount;
              }
            });
          });
        }

        // console.log("totalProductCount", totalProductCount, "rmaExistingCount", rmaExistingCount, "rmas available", totalProductCount - rmaExistingCount);
        // Assert we have enough quantity of product to
        // issue another RMA first. 
        if (totalProductCount - rmaExistingCount >= 1) {
          // OK! Grab the first of the requested product to RMA
          let newRMA = await RMAModel.create({
            rma_product: existingProductsToCount[0].product._id
          });
          // console.log("newRMA", newRMA);
          existingOrder.rma_request.push(newRMA);

          await existingOrder.save()

          res.status(200).json({data: newRMA});
        } else {
          res.status(403).json({errors: ["Max limit of RMAs on this product ID reached."]});
        }

      } else {
        res.status(404).json({errors: ["OrderProduct not found."]});
      }
    } catch(err) {
      console.error("HTTP PUT /admin/order/rma error", err);
      res
        .status(500)
        .json({
          errors: [err]
        });
    }
  }
);

app.post(
  '/order/rma',
  [
    check("_id", "_id field is unprocessable. Min length is 24 chars.").isLength({min: 24}),
    check("rma_tracking", "rma_tracking is unprocessable. Expected string.")
      .optional({checkFalsy: true})
      .isLength({min: 4, max: 32}),
    check("rma_tracking_to_customer", "rma_tracking_to_customer is unprocessable. Expected string.")
      .optional({checkFalsy: true})
      .isLength({min: 4, max: 32}),
    check("rma_tracking_sent_date", "rma_tracking_sent_date is not a date.")
      .optional({checkFalsy: true})
      .isString(),
    check("rma_tracking_to_customer_date", "rma_tracking_to_customer_date is not a date.")
      .optional({checkFalsy: true})
      .isString()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    // console.log("req.body", req.body)
    if (!isValid) {
      res
        .status(422) // Unprocessable entity (has values, but values may be invalid)
        .json({
          errors: errors.array()
        });
      return;
    }

    let RMAModel = getModel("RMA", RMA);
    let _id = req.body._id;
    try {
      let data = {
        rma_tracking: req.body.rma_tracking,
        rma_tracking_to_customer: req.body.rma_tracking_to_customer,
        rma_tracking_sent_date: req.body.rma_tracking_sent_date,
        rma_tracking_to_customer_date: req.body.rma_tracking_to_customer_date
      };
      // console.log("updating RMA with", data);
      let existingRMA = await RMAModel.findByIdAndUpdate(_id, data);

      res
        .status(200)
        .json({data: existingRMA});

    } catch (err) {
      console.error("HTTP POST /admin/order/rma error", err);
      res
        .status(500)
        .json({
          errors: [err]
        });
    }

  }
);

app.post(
  '/order/refund',
  [
    check('_id', 'The _id field is unprocessable.').custom(isID),
    check('reason', "The reason field must be one of 'duplicate', 'fraudulent', or 'requested_by_customer'")
      .optional()
      .custom((value) => {
        let reasons = ['duplicate', 'fraudulent', 'requested_by_customer'];
        return reasons.filter((reason) => { return reason == value}).length > 0;
      }),
    check('orderProducts', 'The orderProducts array is unprocessable')
      .optional({checkFalsy: true})
      .custom((value) => {
        let valid = false;
        let is_array = Array.isArray(value);
        if (is_array) {
          let withIDs = value.filter((_order_product) => {
            return isID(_order_product._id) && isID(_order_product.product._id)
          });

          // After filtering out anything with missing IDs, do the
          // array lengths match?
          if (withIDs.length == value.length) {
            valid = true;
          }
        }

        return valid;
      }),
    check('orderServices', "The orderServices array is unprocessable")
      .optional({checkFalsy: true})
      .custom((value) => {
        // console.log("orderServices", value);
        let valid = false;
        let is_array = Array.isArray(value);
        if (is_array) {
          let withIDs = value.filter((_order_service) => {
            return isID(_order_service._id) && isID(_order_service.service._id)
          });

          // After filtering out anything with missing IDs, do the
          // array lengths match?
          if (withIDs.length == value.length) {
            valid = true;
          }
        }

        return valid;
      })
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    // console.log("req.body", req.body)
    if (!isValid) {
      res
        .status(422) // Unprocessable entity (has values, but values may be invalid)
        .json({
          errors: errors.array()
        });
      return;
    }

    try {
      let id = req.body._id;

      let ProductModel = getModel('Product', Product);
      let ServiceModel = getModel('Service', Service);
      let OrderProductModel = getModel('OrderProduct', OrderProduct);
      let OrderServiceModel = getModel('OrderService', OrderService);
      let RefundModel = getModel('Refund', Refund);
      let RMAModel = getModel('RMA', RMA);
      let OrderModel = getModel('Order', Order);

      let reason = req.body.reason || 'requested_by_customer'; // can be duplicate, fraudulent, or requested_by_customer

      let existingOrder = await OrderModel
        .findById(id)
        .populate({
          path: 'products',
          populate: {
            path: 'product'
          }
        })
        .populate({
          path: 'services',
          populate: 'service'
        })
        .populate({
          path: 'refund_request',
          populate: [
            { path: 'orderProduct' },
            { path: 'OrderService' }
          ]
        });

      if (existingOrder) {
        console.log("HTTP POST /order/refund, existingOrder", existingOrder);

        if (
          existingOrder.payment_gateway == 'stripe' &&
          existingOrder.payment_type
            .filter((type) => {
              return type == 'card'
            })
            .length > 0
        ) {
          let refund_data = {
            payment_intent: existingOrder.payment_id,
            reason: reason
          };

          let refund_amount = 0.00;
          let Refunds = [];

          if (req.body.orderProducts && req.body.orderProducts.length) {

            await Promise.all(req.body.orderProducts.filter(async (bodyOrderProduct) => {
              let fromOrder = await Promise.all(existingOrder.products.filter(async (existingOrderProduct) => {
                let match = existingOrderProduct._id == bodyOrderProduct._id;

                if (match) {
                  let newRefund = await RefundModel.create({
                    refund_amount: (+existingOrderProduct.price),
                    reason: reason,
                    orderProduct: existingOrderProduct
                  });

                  refund_amount += (+existingOrderProduct.price);
                  Refunds.push(newRefund);
                }
                Promise.resolve(match);
                return match;
              }));
              Promise.resolve(fromOrder.length > 0);
              return fromOrder.length > 0;
            }));
          }

          if (req.body.orderServices && req.body.orderServices.length) {
            await Promise.all(req.body.orderServices.filter(async (bodyOrderService) => {
              let fromOrder = await Promise.all(existingOrder.services.filter(async (existingOrderService) => {
                let match = existingOrderService._id == bodyOrderService._id;
                let newRefund = await RefundModel.create({
                  refund_amount: (+existingOrderService.price),
                  reason: reason,
                  orderService: existingOrderService
                });
                if (match) {
                  refund_amount += (+existingOrderService.price);
                  Refunds.push(newRefund);
                }
                Promise.resolve(match);
                return match;
              }));
              Promise.resolve(fromOrder.length > 0);
              return fromOrder.length > 0;
            }));
          }

          if (
            Refunds.length
          ) {
            console.log("Using sum of products and/or services", refund_amount);
            refund_data['amount'] = (refund_amount * 100).toFixed(0);
            
          } else {
            console.log("Using existing order total", existingOrder.total_cost);

            refund_amount = (+existingOrder.total_cost);
            refund_data['amount'] = refund_amount;
          }
          
          console.log("stripe.refunds.create() with data", refund_data);
          let stripeRefund = await stripe.refunds.create(refund_data);

          if (Refunds.length > 0) {
            console.log("Updating per refund item")
            await Promise.all(Refunds.map(async (refund) => {
              refund.refund_id = stripeRefund.id;
              refund.status = stripeRefund.status;
              await refund.save();
              Promise.resolve(refund);
            }));
          } else {
            console.log("Updating whole order refund")
            existingOrder.full_refund_id = stripeRefund.id;
            existingOrder.full_refund_date = new Date();
            existingOrder.full_refund_status = stripeRefund.status;
            await existingOrder.save();
          }
          existingOrder.refund_request = Refunds;
          await existingOrder.save();

          res.status(200).json({status: "ok", order: existingOrder});
        } else {
          res.status(422).json({errors: ["Unsupported payment_gateway on order."]});
        }
      } else {
        res.status(404).json({errors: ["Order not found."]});
      }
    } catch (err) {
      console.error("HTTP POST /admin/order/refund error", err);
      res
        .status(500)
        .json({
          errors: [err]
        });
    }

  }
);


console.log("admin.js LOADED")
module.exports = {
    path: '/api/admin',
    handler: app
}