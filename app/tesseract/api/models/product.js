const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let Product = new Schema({

  id: Number,
  price: Number,
  sku: String,
  name: String,
  inStock: { type: Number, defaultsTo: 0 },
  quantity: Number,
  crowdfundingUrl: String,
  productFeatures: [{ type: Schema.Types.ObjectId, ref: 'ProductFeatures'}],
  pictures: [{type: Schema.Types.ObjectId, ref: 'Images'}]
});

let ProductFeatures = new Schema({
    id: Number,
    url: String
});
export {Product, ProductFeatures}