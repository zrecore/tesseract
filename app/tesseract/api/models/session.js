const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let Session = new Schema({
    token: {type: String, required: true, index: {unique: true}},
    created: {type: Date, default: Date.now()}
});

export default Session