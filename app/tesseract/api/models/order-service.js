const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let OrderService = new Schema({
    id: Number,
    service: {type: Schema.Types.ObjectId, ref: 'Service' },
    sku: String,
    price: Number,
    isService: { type: Boolean, default: true },
    description: String,
    quantity: Number,
    isRefunded: {type: Boolean, default: false}
});
export default OrderService