
import Address from './address.js';
import Region from './region.js';
import Country from './country.js';
import Images from './images.js';
import Shipping from './shipping.js'
import Service from './service.js';
import {Product, ProductFeatures} from './product.js';
import User from './user.js';
import Session from './session.js';
import Role from './role.js';
import OrderProduct from './order-product.js';
import OrderService from './order-service.js';
import Order from './order.js';
import RMA from './rma.js';
import Refund from './refund.js';
import dotenv from 'dotenv';
import mongoose from 'mongoose';

const startMongoose = () => {
    dotenv.config();
    mongoose.connect(
      `mongodb://${process.env.DB_HOST}${process.env.DB_PORT ? ':' + process.env.DB_PORT : ''}/${process.env.DB_NAME}`,
      { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false}
    );
};

const getModel = (modelName, schema) => {
  return mongoose.models[modelName] ?
    mongoose.model(modelName) :
    mongoose.model(modelName, schema);
}

export {
  ProductFeatures,
  Address,
  Region,
  Country,
  Images,
  Shipping,
  Service,
  Product,
  User,
  Session,
  OrderProduct,
  OrderService,
  Order,
  Role,
  RMA,
  Refund,
  getModel,
  startMongoose
};    