const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let Service = new Schema({
    id: Number,
    sku: String,
    price: Number,
    isService: { type: Boolean, default: true },
    pictures: {type: Schema.Types.ObjectId, ref: 'Images'},
    description: String,
    quantity: Number
});
export default Service