const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let Address = new Schema({
  id: Number,
  country: { type: Schema.Types.ObjectId, ref: 'Country'},
  fullName: String,
  email: String,
  address1: String,
  address2: String,
  cityTown: String,
  state: { type: Schema.Types.ObjectId, ref: 'Region'},
  postalZipCode: String,
});
export default Address