const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let Country = new Schema({
  code: String,
  country: String,
});
export default Country