const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let Role = new Schema({
  name: String,
  weight: {type: Number, default: 0}
});
export default Role