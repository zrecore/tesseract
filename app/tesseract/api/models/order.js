const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

const Order = new Schema({
  id: Number,
  billing: { type: Schema.Types.ObjectId, ref: 'Address' },
  shipping: { type: Schema.Types.ObjectId, ref: 'Address' },
  products: [{type: Schema.Types.ObjectId, ref: 'OrderProduct'}],
  services: [{type: Schema.Types.ObjectId, ref: 'OrderService'}],
  payment_type: [String],
  payment_gateway: String,
  payment_id: String,
  shipping_cost: Number,
  item_cost: Number,
  total_cost: Number,
  currency: String,
  receipt_email: String,
  order_date: {type: Date, default: Date.now()},
  ship_method: { type: Schema.Types.ObjectId, ref: 'Shipping'},
  ship_date: Date,
  ship_tracking: String,
  full_refund_id: String,
  full_refund_date: Date,
  full_refund_status: String,
  refund_request: [{type: Schema.Types.ObjectId, ref: 'Refund'}],
  rma_request: [{type: Schema.Types.ObjectId, ref: 'RMA'}]
});

export default Order;