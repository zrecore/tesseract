const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let RMA = new Schema({
  rma_date: {type: Date, default: Date.now()},
  rma_product: {type: Schema.Types.ObjectId, ref: 'Product'},
  rma_tracking: String,
  rma_tracking_sent_date: Date,
  rma_tracking_to_customer: String,
  rma_tracking_to_customer_date: Date,
});
export default RMA