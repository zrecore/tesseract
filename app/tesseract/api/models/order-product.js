const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let OrderProduct = new Schema({

  id: Number,
  product: {type: Schema.Types.ObjectId, ref: 'Product'},
  price: Number,
  sku: String,
  name: String,
  quantity: Number,
  crowdfundingUrl: String,
  isRefunded: {type: Boolean, default: false}
});


export default OrderProduct;