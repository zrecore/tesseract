const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;
var bcrypt = require('bcrypt');

bcrypt = require('bcrypt');

let User = new Schema({
    username: {type: String, required: true, index: {unique: true}},
    email: {type: String, required: true, index: {unique: true}},
    password: {type: String},
    role: {type: ObjectId}
});

// See https://stackoverflow.com/questions/14588032/mongoose-password-hashing
User.pre('save', async function (next) {
    // console.log("UserSchema pre, user", this);
    if (! (this.password && this.isModified('password')) ) return next();

    let salt = await bcrypt.genSalt(+process.env.BCRYPT_SALT_ROUNDS);
    let hash = await bcrypt.hash(this.password, salt);
    
    this.password = hash;
});

User.methods.comparePassword = async function (candidatePassword) {
    // console.log("comparing", candidatePassword, this.password);
    return bcrypt.compare(candidatePassword, this.password);
}
export default User