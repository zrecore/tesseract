const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;
let Shipping = new Schema({
    id: Number,
    price: Number,
    value: String,
    description: String,
});
export default Shipping