const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let Region = new Schema({
    countryCode: String,
    code: String,
    name: String,
    type: String,
});
export default Region