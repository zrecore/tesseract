const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let Refund = new Schema({
  refund_date: {type: Date, default: Date.now()},
  refund_amount: Number,
  reason: String,
  status: String,
  refund_method: String,
  refund_id: String,
  orderProduct: {type: Schema.Types.ObjectId, ref: 'OrderProduct', required: false},
  orderService: {type: Schema.Types.ObjectId, ref: 'OrderService', required: false}
});
export default Refund