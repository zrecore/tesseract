const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

let Images = new Schema({
    id: Number,
    "file_name": String,
    title: String,
});
export default Images