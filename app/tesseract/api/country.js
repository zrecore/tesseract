var express = require('express');
var bodyParser = require('body-parser');
var axios = require('axios');
var rateLimit = require('express-rate-limit');
var cors = require('cors');
var dotenv = require('dotenv');
var compression = require('compression');
var helmet = require('helmet');

var country = require('./mock/country');

// const {
//   check,
//   validationResult
// } = require('express-validator');

// const {
//   sanitizeBody
// } = require('express-validator');
const {
  expressCspHeader,
  INLINE,
  NONE,
  SELF
} = require('express-csp-header');

// const sgMail = require('@sendgrid/mail');
const limiter = rateLimit({
    windowsMs: 30 * 60 * 1000, // 30 minutes
    max: 100//1 // limit each IP to 1 requests per windowMs
});
const app = express();

dotenv.config();

app.use(helmet());
app.use(cors());
app.options(
  process.env.CORS_DOMAINS,
  cors()
);

app.use(limiter);
app.use(expressCspHeader({
  "default-src": [NONE],
  "image-src": [SELF]
}));
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());

/**
 * Send a contact request via SendGrid API (e-mail).
 * Expects name, email, and message strings.
 * 
 */
app.get(
  '/',
  [
    // check("countryCode", "countryCode max length exceeded").isLength({max: 2}),
    // check("email", "email field is not a valid e-mail").isEmail(),
    // check("message", "message field is invalid (min 3 chars, max 1000 chars)").isLength({min: 3, max: 1000})
  ],
  (req, res) => {
    let result;
    let data;

    // console.log('/country')
    data = country.default
    result = res
      .status(200)
      .json(data)
    return result;
});

console.log("country.js LOADED")
module.exports = {
    path: '/api/country',
    handler: app
}