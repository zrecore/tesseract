var express = require('express');
var bodyParser = require('body-parser');
var axios = require('axios');
var rateLimit = require('express-rate-limit');
var cors = require('cors');
var dotenv = require('dotenv');
var compression = require('compression');
var helmet = require('helmet');
var xml2js = require('xml2js');

var shippingMethods = require('./mock/shipping');

/**
 * See https://www.usps.com/business/web-tools-apis/general-api-developer-guide.pdf
 * See https://www.usps.com/business/web-tools-apis/technical-documentation.htm
 */

const {
  check,
  validationResult
} = require('express-validator');

const {
  sanitizeBody
} = require('express-validator');
const {
  expressCspHeader,
  INLINE,
  NONE,
  SELF
} = require('express-csp-header');

// const sgMail = require('@sendgrid/mail');
const limiter = rateLimit({
    windowsMs: 30 * 60 * 1000, // 30 minutes
    max: 100//1 // limit each IP to 1 requests per windowMs
});
const app = express();

dotenv.config();

app.use(helmet());
app.use(cors());
app.options(
  process.env.CORS_DOMAINS,
  cors()
);

app.use(limiter);
app.use(expressCspHeader({
  "default-src": [NONE],
  "image-src": [SELF]
}));
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());
// app.get('/', (req, res) => {
//     return res.status(200).json({"hello": "world"})
// })
/**
 * Send a contact request via SendGrid API (e-mail).
 * Expects name, email, and message strings.
 * 
 */
const USPS_USER_ID = process.env.USPS_USER_ID;

function isAddressNotFound(result) {
  // console.log("isAddressNotFound", result)
  return result.AddressValidateResponse.Address &&
         result.AddressValidateResponse.Address.Error;
}

function isError(result) {
  return result.Error;
}

app.post(
  '/address',
  [
    check("address1", "address1 field is not valid").isLength({min: 1, max: 1000}),
    check("city", "city field is not valid").isLength({min: 1, max: 1000}),
    check("state", "state field is not valid").isLength({min: 1, max: 1000})
  ],
  (req, res, next) => {
    // console.log(req.body);
    let result;
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    // console.log("isValid?", isValid);
    if (isValid) {
      let xml = '<?xml version="1.0" encoding="UTF-8"?>' +
        '<AddressValidateRequest USERID="' + USPS_USER_ID + '">' +
        '  <Address ID="0">' +
        '    <Address1>' + req.body.address1 + '</Address1>' +
        '    <Address2>' + (req.body.address2 ? req.body.address2 : '') + '</Address2>' +
        '    <City>' + req.body.city + '</City>' +
        '    <State>' + (req.body.state ?  req.body.state : '')  + '</State>' +
        '    <Zip5>' + (req.body.zip5 ? req.body.zip5 : '') + '</Zip5>' +
        '    <Zip4>' + (req.body.zip4 ?  req.body.zip4 : '') + '</Zip4>' + 
        '  </Address>' +
        '</AddressValidateRequest>';

      // console.log(xml);
      result = axios.post(
        'https://secure.shippingapis.com/ShippingAPITest.dll?API=Verify&XML=' + xml,
        {},
        {
          "headers": {
            "Content-Type": "text/xml",
            "post": {
              "Content-Type": "text/xml"
            }
          }
        }
      ).then((apiResponse) => {

        let parser = new xml2js.Parser({mergeAttrs: true, trim: true, explicitArray: false});
        parser.parseStringPromise(apiResponse.data).then((result) => {
          // res.status(200).json(result);
          if (!isAddressNotFound(result) && !isError(result)) {
            res.status(200).json(result);
          } else {
            if (isAddressNotFound(result)) {
              res.status(400).json({errors: [result.AddressValidateResponse.Address.Error.Description]})
            } else {
              res.status(400).json({errors: [result.Error.Description]});
            }
          }
        });
      });
    } else {
      // res.status(400); // Bad Request (missing value? Malformed syntax?)
      result = res
        .status(422) // Unprocessable entity (has values, but values may be invalid)
        .json({
          errors: errors.array()
        });
    }
    

    return result;
  }
);
app.get(
  '/',
  // [
  //   check("name", "name field is empty").isLength({min: 1}),
  //   check("email", "email field is not a valid e-mail").isEmail(),
  //   check("message", "message field is invalid (min 3 chars, max 1000 chars)").isLength({min: 3, max: 1000})
  // ],
  (req, res) => {
    let result;
    
    result = res
      .status(200)
      .json(shippingMethods)
    return result;
});

console.log("shipping.js LOADED")
module.exports = {
    path: '/api/shipping',
    handler: app
}