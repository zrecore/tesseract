var express = require('express');
var bodyParser = require('body-parser');
var axios = require('axios');
var rateLimit = require('express-rate-limit');
var cors = require('cors');
var dotenv = require('dotenv');
var compression = require('compression');
var helmet = require('helmet');

var country = require('./mock/country');
var region = require('./mock/region');

const {
  check,
  validationResult
} = require('express-validator');

const {
  sanitizeBody
} = require('express-validator');
const {
  expressCspHeader,
  INLINE,
  NONE,
  SELF
} = require('express-csp-header');

// const sgMail = require('@sendgrid/mail');
const limiter = rateLimit({
    windowsMs: 30 * 60 * 1000, // 30 minutes
    max: 100//1 // limit each IP to 1 requests per windowMs
});
const app = express();

dotenv.config();

app.use(helmet());
app.use(cors());
app.options(
  process.env.CORS_DOMAINS,
  cors()
);

app.use(limiter);
app.use(expressCspHeader({
  "default-src": [NONE],
  "image-src": [SELF]
}));
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());

/**
 * Send a contact request via SendGrid API (e-mail).
 * Expects name, email, and message strings.
 * 
 */


app.get(
  '/',
  [
    check("countryCode", "countryCode max length exceeded").isLength({max: 2})
  ],
  (req, res) => {
    let result;
    let data;
    const errors = validationResult(req);
    // console.log("/api/region", req.query)
    if(typeof req.query.countryCode !== 'undefined' || error) {
      // console.log("query", req.query);
      // console.log("country len", country.length);
      // console.log("region len", region.length);
      data = Object.assign({}, country.default.filter((entry) => entry.code == req.query.countryCode));
      // console.log(country)


      data = Object.assign({}, data[0], {
        "regions": region.default.filter((entry) => entry.countryCode == req.query.countryCode)
      });
      // console.log('data is now', data);

      result = res
        .status(200)
        .json(data)
    } else {
      result = res
        .status(404)
        .json({
          errors: ["Invalid country code"]
        })
    }
    
    return result;
});
console.log("region.js LOADED")
module.exports = {
    path: '/api/region',
    handler: app
}