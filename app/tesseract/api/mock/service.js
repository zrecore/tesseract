export default [
    {
      id: 3,
      sku: 'sku_test_0000000002',
      price: 100.00,
      isService: true,
      description: 'Service Sample A',
      pictures: [{
        id: 1,
        "file_name": 'images/placeholder-800x800.svg',
        title: 'Sample Service Picture'
      }]
    },
    {
      id: 4,
      sku: 'sku_test_0000000003',
      price: 200.00,
      isService: true,
      description: 'Service Sample B',
      pictures: [{
        id: 1,
        "file_name": 'images/placeholder-800x800.svg',
        title: 'Sample Service Picture'
      }]
    }
]