export default [
    {
      id: 1,
      price: 0.00,
      value: 'free',
      description: 'FREE SHIPPING (3-7 days)'
    },
    {
      id: 2,
      price: 8.99,
      value: 'courrier_next_day',
      description: 'Courrier Next Day (1 day)'
    },
    {
      id: 3,
      price: 14.99,
      value: 'ups_next_day',
      description: 'UPS Next Day (1 days)'
    }
]