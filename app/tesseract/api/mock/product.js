export default [
    {
      id: 1,
      price: 10.00,
      sku: 'sku_test_0000000001',
      name: 'Sample 1',
      inStock: 1,
      pictures: [
        {id: 1, "file_name": '/images/placeholder-800x800.svg', title: 'Sample Product Picture'},
      ],
      productFeatures: [
        {
          id: 1,
          title: 'Next Day',
          url: '/images/arrow-right.png',
        },
      ],
      description: 'Sample product description.'
    },
]