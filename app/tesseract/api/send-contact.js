var express = require('express');
var bodyParser = require('body-parser');
var axios = require('axios');
var rateLimit = require('express-rate-limit');
var cors = require('cors');
var dotenv = require('dotenv');
var compression = require('compression');
var helmet = require('helmet');

const {
  check,
  validationResult
} = require('express-validator');

const {
  sanitizeBody
} = require('express-validator');
const {
  expressCspHeader,
  INLINE,
  NONE,
  SELF
} = require('express-csp-header');

const sgMail = require('@sendgrid/mail');
const limiter = rateLimit({
    windowsMs: 30 * 60 * 1000, // 30 minutes
    max: 10//1 // limit each IP to 1 requests per windowMs
});
const app = express();

dotenv.config();

app.use(helmet());
app.use(cors());
app.options(
  process.env.CORS_DOMAINS,
  cors()
);

app.use(limiter);
app.use(expressCspHeader({
  "default-src": [NONE],
  "image-src": [SELF]
}));
app.use(compression());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());
// app.get('/', (req, res) => {
//     return res.status(200).json({"hello": "world"})
// })
/**
 * Send a contact request via SendGrid API (e-mail).
 * Expects name, email, and message strings.
 * 
 */
app.post(
  '/',
  [
    check("name", "name field is empty").isLength({min: 1}),
    check("email", "email field is not a valid e-mail").isEmail(),
    check("message", "message field is invalid (min 3 chars, max 1000 chars)").isLength({min: 3, max: 1000})
  ],
  (req, res) => {
    let result;
    const errors = validationResult(req);
    const isValid = errors.isEmpty(); // validate first!
    
    if (isValid) {
      const sendgridKey = process.env.SENDGRID_KEY;
      // console.log(req.body);
      const data = {
        "to": process.env.SUPPORT_EMAIL,
        "from": '"' + req.body.name + '" ' + '<' + req.body.email + '>',
        "subject": process.env.SUPPORT_SUBJECT,
        "text": req.body.message,
        "html": "<p>" + req.body.message + "</p>"
      };
      // console.table(data);
      sgMail.setApiKey(sendgridKey);
      sgMail
        .send(data)
        .then((resp) => {
          result = res.status(202).json({message: 'Processed POST HTTP method /send-contact.'});
          // console.log("send-contact API OK", resp);
        })
        .catch((error) => {
          result = res.status(500).json({errors: ["send-contact API Error: (" + error.code + ") " + error.message]});
          // console.log("send-contact API Error", error.code, error.message);
        });
    } else {
        // res.status(400); // Bad Request (missing value? Malformed syntax?)
        result = res
          .status(422) // Unprocessable entity (has values, but values may be invalid)
          .json({
            errors: errors.array()
          });
    }
    return result;
});
console.log("send-contact.js LOADED")
module.exports = {
    path: '/api/send-contact',
    handler: app
}