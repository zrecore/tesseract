export default {
    "default": [{
        "id": 1,
        "price": 0,
        "value": "free",
        "description": "FREE SHIPPING (3-15 days)"
    }, {
        "id": 2,
        "price": 8.99,
        "value": "usps_firstclass",
        "description": "USPS First Class (6-10 days)"
    }, {
        "id": 3,
        "price": 14.99,
        "value": "usps_priority",
        "description": "USPS Priority Mail (2-5 days)"
    }]
}