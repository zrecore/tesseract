export default {
    "products": [{
        "isRefunded": false,
        "_id": "5efed7638383180363316e40",
        "id": 4,
        "sku": "sku_1234567890",
        "name": "Sample Product",
        "price": 20,
        "quantity": 1,
        "product": {
            "productFeatures": [{
                "_id": "5ef40473dccda801df54b94a",
                "id": 2,
                "url": "images/placeholder-800x800.svg",
                "__v": 0
            }],
            "pictures": [{
                "_id": "5ef40473dccda801df54b948",
                "id": 7,
                "file_name": "images/placeholder-800x800.svg",
                "title": "Sample Product Image",
                "__v": 0
            }],
            "_id": "5ef40478dccda801df54cc45",
            "id": 4,
            "sku": "sku_0000000001",
            "name": "Sample Product",
            "price": 20,
            "inStock": 1,
            "__v": 0
        },
        "__v": 0
    }],
    "services": [],
    "payment_type": ["card"],
    "order_date": "2020-07-03T06:59:47.321Z",
    "refund_request": [],
    "rma_request": [],
    "_id": "5efed7638383180363316e43",
    "billing": {
        "_id": "5efed7638383180363316e41",
        "country": {
            "_id": "5ef40473dccda801df54b9c7",
            "code": "US",
            "country": "United States",
            "__v": 0
        },
        "fullName": "Test User",
        "address1": "1 MARKET ST",
        "cityTown": "SAN FRANCISCO",
        "state": {
            "_id": "5ef40475dccda801df54cb42",
            "countryCode": "US",
            "code": "CA",
            "name": "California",
            "type": "State",
            "__v": 0
        },
        "postalZipCode": "94105-1420",
        "email": "test@example.com",
        "__v": 0
    },
    "shipping": {
        "_id": "5efed7638383180363316e42",
        "country": {
            "_id": "5ef40473dccda801df54b9c7",
            "code": "US",
            "country": "United States",
            "__v": 0
        },
        "fullName": "Test User",
        "address1": "1 MARKET ST",
        "cityTown": "SAN FRANCISCO",
        "state": {
            "_id": "5ef40475dccda801df54cb42",
            "countryCode": "US",
            "code": "CA",
            "name": "California",
            "type": "State",
            "__v": 0
        },
        "postalZipCode": "94105-1420",
        "email": "test@example.com",
        "__v": 0
    },
    "payment_gateway": "stripe",
    "payment_id": "pi_0123456789012345678901234",
    "shipping_cost": 0,
    "item_cost": 20,
    "total_cost": 2000,
    "currency": "usd",
    "receipt_email": "test@example.com",
    "ship_method": {
        "_id": "5ef40478dccda801df54cc3a",
        "id": 1,
        "price": 0,
        "value": "free",
        "description": "FREE SHIPPING (3-15 days)",
        "__v": 0
    },
    "__v": 0
}