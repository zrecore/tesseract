export default {
    "count": 1,
    "data": [{
        "products": [{
            "isRefunded": false,
            "_id": "5efd370fe84536021c42a4ec",
            "id": 1,
            "price": 29.99,
            "sku": "sku_01234567890",
            "name": "Sample Product",
            "quantity": 1,
            "product": "5ef40478dccda801df54cc42",
            "__v": 0
        }],
        "services": [],
        "payment_type": ["card"],
        "order_date": "2020-07-01T18:53:25.754Z",
        "refund_request": [],
        "rma_request": ["5efed1778383180363316e3e"],
        "_id": "5efd370fe84536021c42a4ef",
        "billing": "5efd370fe84536021c42a4ed",
        "shipping": "5efd370fe84536021c42a4ee",
        "payment_gateway": "stripe",
        "payment_id": "pi_0123456789012345678901234",
        "shipping_cost": 0,
        "item_cost": 29.99,
        "total_cost": 2999,
        "currency": "usd",
        "receipt_email": "test@example.com",
        "ship_method": "5ef40478dccda801df54cc3a",
        "__v": 1,
        "ship_date": "2020-06-24T00:00:00.000Z",
        "ship_tracking": "LZ88887777"
    }]
}