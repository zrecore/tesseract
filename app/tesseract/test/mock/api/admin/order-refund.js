export default {
    "count": 1,
    "data": [{
        "products": [{
            "isRefunded": false,
            "_id": "5efd36f0e84536021c42a4e8",
            "id": 1,
            "sku": "sku_0000000001",
            "name": "Sample Product",
            "price": 29.99,
            "quantity": 1,
            "product": "5ef40478dccda801df54cc43",
            "__v": 0
        }],
        "services": [],
        "payment_type": ["card"],
        "order_date": "2020-07-01T18:53:25.754Z",
        "refund_request": [],
        "rma_request": [],
        "_id": "5efd36f0e84536021c42a4eb",
        "billing": "5efd36f0e84536021c42a4e9",
        "shipping": "5efd36f0e84536021c42a4ea",
        "payment_gateway": "stripe",
        "payment_id": "pi_123456789012345678901234",
        "shipping_cost": 0,
        "item_cost": 29.99,
        "total_cost": 2999,
        "currency": "usd",
        "receipt_email": "test@example.com",
        "ship_method": "5ef40478dccda801df54cc3a",
        "__v": 0,
        "full_refund_date": "2020-07-03T06:39:40.009Z",
        "full_refund_id": "re_012345678912345678901234",
        "full_refund_status": "succeeded"
    }]
}