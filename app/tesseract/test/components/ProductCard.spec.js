import {
    mount,
    createLocalVue
} from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import ProductCard from '@/components/ProductCard.vue'

describe('ProductCard', () => {
  let wrapper;
  let instance;

  beforeEach(() => {
      const vuetify = new Vuetify()
      Vue.use(Vuetify)
      const localVue = createLocalVue()
      localVue.use(vuetify)

      wrapper = mount(ProductCard, {
          localVue,
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {
    expect(wrapper).toBeTruthy()
  })

  // test('is loadOrders() call rendinger orders of type "refund" correctly', async () => {
  //     const mock = new MockAdapter(wrapper.vm.$axios)
  //     mock.onGet('/api/admin/order').reply(200, mockOrderRefund)
  //     await instance.loadOrders()

  //     expect(instance.system_count).toBe(1)
  //     expect(instance.system_orders.length).toBe(1)
  //     expect(instance.systemCount).toBe(1)
  //     expect(instance.systemOrders.length).toBe(1)

  //     wrapper.vm.$nextTick(() => {
  //       let cardTexts = wrapper.findAll('.v-card__text')
  //       let divTexts = wrapper.findAll('.v-card__text div.order-timestamp')
  //       expect(cardTexts.length).toBe(1)
  //       expect(divTexts.length).toBe(1)
  //     })
  // })
})