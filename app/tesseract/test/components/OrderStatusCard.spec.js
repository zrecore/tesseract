import {
    mount,
    createLocalVue
} from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import axios from '@/plugins/axios.js'
import MockAdapter from 'axios-mock-adapter'
import eventBus from '@/plugins/event-bus.js'
import OrderStatusCard from '@/components/OrderStatusCard.vue'

import mockOrderRefund from '@/test/mock/api/admin/order-refund.js'
import mockOrderRma from '@/test/mock/api/admin/order-rma.js'
import mockOrderUnshipped from '@/test/mock/api/admin/order-unshipped.js'
import moment from 'moment'

describe('OrderStatusCard', () => {
  let wrapper;
  let instance;

  beforeEach(() => {
      const vuetify = new Vuetify()
      Vue.use(Vuetify)
      Vue.prototype.$moment = moment
      const localVue = createLocalVue()
      localVue.use(vuetify)
      localVue.use(axios)
      localVue.use(eventBus)


      wrapper = mount(OrderStatusCard, {
          localVue,
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {
    expect(wrapper).toBeTruthy()
  })

  test('is loadOrders() call rendinger orders of type "refund" correctly', async () => {
      const mock = new MockAdapter(wrapper.vm.$axios)
      mock.onGet('/api/admin/order').reply(200, mockOrderRefund)
      await instance.loadOrders()

      expect(instance.system_count).toBe(1)
      expect(instance.system_orders.length).toBe(1)
      expect(instance.systemCount).toBe(1)
      expect(instance.systemOrders.length).toBe(1)

      wrapper.vm.$nextTick(() => {
        let cardTexts = wrapper.findAll('.v-card__text')
        let divTexts = wrapper.findAll('.v-card__text div.order-timestamp')
        expect(cardTexts.length).toBe(1)
        expect(divTexts.length).toBe(1)
      })
  })
})
