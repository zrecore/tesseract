import {
    mount,
    createLocalVue,
    config
} from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import CheckoutComponent from '@/pages/checkout.vue'
import axios from '@/plugins/axios.js'
import MockAdapter from 'axios-mock-adapter'

import {state, mutations, getters} from '@/store/index.js'
import mockShipping from '@/test/mock/api/shipping.js'
import mockCountry from '@/test/mock/api/country.js'
import mockStripeCardIntent from '@/test/mock/api/process/stripe-card-intent.js'

describe('pages/checkout.vue', () => {
  let wrapper
  let instance

  beforeEach(() => {
      // const vuetify = new Vuetify()
      // const localVue = createLocalVue()

      Vue.prototype.$stripe = {
        apiStripeCardIntent: () => {}
      },
      Vue.prototype.$store = {
              state: {
                cart: [
                  {sku: 'sku_HNljew0oQ0pzkx', quantity: 1},
                ]
              },
              mutations: mutations,
              getters: getters
            };
      Vue.use(Vuetify)
      // Vue.use(vuetify)
      Vue.use(axios)

      wrapper = mount(CheckoutComponent, {
          created() {
            this.$vuetify.lang = {
              t: () => {}
            }

            this.$vuetify.theme = {dark: true}
          },
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {

    const mock = new MockAdapter(wrapper.vm.$axios)
    mock.onGet('/api/shipping').reply(200, mockShipping)
    mock.onGet('/api/country').reply(200, mockCountry)
    mock.onGet('/api/process/stripe-card-intent').reply(200, mockStripeCardIntent)
    // await instance.loadOrders()
    wrapper.vm.$nextTick(() => {
      // expect(instance.validate()).toBeTruthy()
      expect(wrapper).toBeTruthy()
    })
    
    
  })


})