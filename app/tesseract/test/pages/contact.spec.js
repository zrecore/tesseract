import {
    mount,
    createLocalVue,
    config
} from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import ContactComponent from '@/pages/contact.vue'
import axios from '@/plugins/axios.js'
import MockAdapter from 'axios-mock-adapter'

// import {state, mutations, getters} from '@/store/index.js'

describe('pages/contact.vue', () => {
  let wrapper
  let instance

  beforeEach(() => {
      const vuetify = new Vuetify()
      const localVue = createLocalVue()

      // Vue.prototype.$store = {
      //         state: state(),
      //         mutations: mutations,
      //         getters: getters
      //       };
      Vue.use(Vuetify)
      localVue.use(vuetify)
      localVue.use(axios)

      wrapper = mount(ContactComponent, {
          localVue,
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {
      
    expect(wrapper).toBeTruthy()
  })

  test('computed properties work for name, email, message', () => {

    // const mock = new MockAdapter(wrapper.vm.$axios)
      // mock.onGet('/api/admin/order').reply(200, mockOrderRefund)
      // await instance.loadOrders()
    instance.nameValue = 'test'
    instance.emailValue = 'test@example.com'
    instance.messageValue = 'Test Message'
      
    expect(instance.name).toBeTruthy()
    expect(instance.email).toBeTruthy()
    expect(instance.message).toBeTruthy()
  })

  test('form validates with valid name, email, and message', () => {

    instance.nameValue = 'test'
    instance.emailValue = 'test@example.com'
    instance.messageValue = 'Test Message'

    wrapper.vm.$nextTick(() => {
      expect(instance.validate()).toBeTruthy()
    })
    
    
  })
})