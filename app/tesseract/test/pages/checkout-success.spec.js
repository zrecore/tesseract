import {
    mount,
    createLocalVue
} from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import CheckoutSuccessComponent from '@/pages/checkout-success.vue'

describe('pages/checkout-success.vue', () => {
  let wrapper
  let instance

  beforeEach(() => {
      const vuetify = new Vuetify()
      Vue.use(Vuetify)
      const localVue = createLocalVue()
      localVue.use(vuetify)

      wrapper = mount(CheckoutSuccessComponent, {
          localVue,
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {
    expect(wrapper).toBeTruthy()
  })
})