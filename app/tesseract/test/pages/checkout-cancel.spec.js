import {
    mount,
    createLocalVue
} from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import CheckoutCancelComponent from '@/pages/checkout-cancel.vue'

describe('pages/checkout-cancel.vue', () => {
  let wrapper
  let instance

  beforeEach(() => {
      const vuetify = new Vuetify()
      Vue.use(Vuetify)
      const localVue = createLocalVue()
      localVue.use(vuetify)

      wrapper = mount(CheckoutCancelComponent, {
          localVue,
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {
    expect(wrapper).toBeTruthy()
  })
})