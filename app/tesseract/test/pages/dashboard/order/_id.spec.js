import {
    mount,
    createLocalVue,
    config
} from '@vue/test-utils'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import OrderIdComponent from '@/pages/dashboard/order/_id.vue'
import axios from '@/plugins/axios.js'
import MockAdapter from 'axios-mock-adapter'

import mockOrderSingle from '@/test/mock/api/admin/order-single.js'

describe('pages/dashboard/order/_id.vue', () => {
  let wrapper
  let instance
  let router
  let orderId
  beforeEach(() => {
      const vuetify = new Vuetify()
      const localVue = createLocalVue()
      router = new VueRouter()

      orderId = '5efed7638383180363316e43'

      Vue.use(Vuetify)
      localVue.use(vuetify)
      localVue.use(axios)
      localVue.use(VueRouter)
      router.push({path: `/dashshboard/order/${orderId}`})

      wrapper = mount(OrderIdComponent, {
          localVue,
          router,
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {
    const mock = new MockAdapter(wrapper.vm.$axios)
    mock.onGet('/api/admin/order').reply(200, mockOrderSingle)
    expect(wrapper).toBeTruthy()
  })
})