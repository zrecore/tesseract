import {
    mount,
    createLocalVue,
    config
} from '@vue/test-utils'
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import IndexComponent from '@/pages/dashboard/index.vue'
import axios from '@/plugins/axios.js'
import MockAdapter from 'axios-mock-adapter'

import mockOrder from '@/test/mock/api/admin/order.js'

describe('pages/dashboard/index.vue', () => {
  let wrapper
  let instance
  let router
  beforeEach(() => {
      const vuetify = new Vuetify()
      const localVue = createLocalVue()
      router = new VueRouter()

      Vue.use(Vuetify)
      localVue.use(vuetify)
      localVue.use(axios)
      localVue.use(VueRouter)
      router.push({path: '/dashshboard/', query: {}})

      wrapper = mount(IndexComponent, {
          localVue,
          router,
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {
    const mock = new MockAdapter(wrapper.vm.$axios)
    mock.onGet('/api/admin/order').reply(200, mockOrder)
    expect(wrapper).toBeTruthy()
  })
})