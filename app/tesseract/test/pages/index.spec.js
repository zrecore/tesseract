import {
    mount,
    createLocalVue,
    config
} from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import IndexComponent from '@/pages/index.vue'
import axios from '@/plugins/axios.js'
import MockAdapter from 'axios-mock-adapter'

describe('pages/index.vue', () => {
  let wrapper
  let instance

  beforeEach(() => {
      const vuetify = new Vuetify()
      const localVue = createLocalVue()

      Vue.use(Vuetify)
      localVue.use(vuetify)
      localVue.use(axios)

      wrapper = mount(IndexComponent, {
          localVue,
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {
      
    expect(wrapper).toBeTruthy()
  })
})