import {
    mount,
    createLocalVue
} from '@vue/test-utils'
import Vue from 'vue'
import Vuetify from 'vuetify'
import AboutComponent from '@/pages/about.vue'

describe('pages/about.vue', () => {
  let wrapper
  let instance

  beforeEach(() => {
      const vuetify = new Vuetify()
      Vue.use(Vuetify)
      const localVue = createLocalVue()
      localVue.use(vuetify)

      wrapper = mount(AboutComponent, {
          localVue,
          stubs: ["router-link"]
      })
      instance = wrapper.vm
  })
  test('is a Vue instance', () => {
    expect(wrapper).toBeTruthy()
  })
})